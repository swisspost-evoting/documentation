# Google Chrome Mobile

## Deleting the browser cache in iOS



1. Click on the **"three dots"**, "Customise and manage Google Chrome", marked with the arrow in the screenshot.
2. Select the menu item **"Settings"**.
3. Select the menu item **"Privacy and security"**.


4. Now click on **"Delete browser data“** in this screen under **”Privacy and security"**.
5. In the drop-down list, select at least the time period that covers your electronic voting process. For example, select the entry **"Last hour"**.
6. Select the checkboxes **"Cookies and other website data"** and **"Cached images and files"**.
7. Click on the menu button **"Delete data"**.

**Screenshot Step: 1**

![Screenshot Step: 1](../img/rules/chrome/de/chrome_mobile_incognito.png)