# Sicherheitsratschläge

Die Stimmberechtigten können selbst den korrekten Verlauf bei der elektronischen Stimmabgabe sicherstellen, indem sie den Prozess gemäss Anleitung durchführen. Dazu gehört insbesondere, dass sie die Prüfcodes, die vor der Stimmabgabe generiert werden, mit den auf den physischen Stimmunterlagen erhaltenen Prüfcodes abgleichen. Um die Stimme zu übermitteln, wird der Bestätigungscode von den physischen Stimmunterlagen auf das verwendete Gerät abgetippt. Auch der Finalisierungscode, der nach der Übermittlung der Stimme angezeigt wird, muss mit dem Finalisierungscode auf den physischen Stimmunterlagen abgeglichen werden. Dieses Verfahren heisst individuelle Verifizierbarkeit. Damit können die Stimmberechtigten überprüfen, ob ihre Stimme unverändert übermittelt und korrekt in der elektronischen Urne registriert worden ist.

Zusätzlich können Stimmberechtigte verschiedene Prüfungen an dem für die Stimmabgabe verwendeten Gerät vornehmen, um sicherzugehen, dass die eigene Stimme nicht manipuliert wird.

Nachstehend sind die verschiedenen Massnahmen beschrieben.

## Überprüfung des Fingerabdruck des Zertifikats

Wenn Sie überprüfen möchten, ob Sie auf dem korrekten, offiziellen E-Voting-Portal sind, können Sie den Fingerabdruck des Zertifikats überprüfen. Tun Sie dies vor der elektronischen Stimmabgabe, können Sie ausschliessen, dass Sie sich auf einer manipulierten oder gefälschten Seite befinden. **Bitte beachten Sie, dass iOS-Geräte dies derzeit nicht unterstützen**.

| [![google-chrome](../icons/google-chrome.png)](zert_chrome.md) | [![Microsoft-Edge](../icons/microsoft-edge.png)](zert_edge.md) | [![firefox](../icons/firefox.png)](zert_firefox.md) | [![apple-safari](../icons/apple-safari.png)](zert_safari.md) |
| ----------------------------------------- | ------------------------------------------------------------ | ---------------------------------------- |-------------------------------------------------------------|
| [Google Chrome](zert_chrome.md)           | [Edge](zert_edge.md)                                         | [Firefox](zert_firefox.md)               | [Safari](zert_safari.md)                                     |    [Safari Mobile](del_safarimobile.md)    |

### Was mache ich, wenn mein Browser einen falschen Fingerabdruck anzeigt?

Sie sollten den Stimmprozess abbrechen und die zuständige Stelle beim Kanton (Supportteam) informieren.

Ein falsch angezeigter Fingerabdruck des Webseitenzertifikats bedeutet, dass keine direkte Verbindung zwischen dem Webbrowser und der Stimmplattform besteht. Die Unterbrechung wird in den meisten Fällen nicht mit einem Missbrauchsversuch sondern im Gegenteil mit einer Schutzmassnahme in Verbindung stehen. Beispielsweise unterbrechen bestimmte Unternehmen in ihren Netzwerken die Verbindung zwischen den Computern der Mitarbeitenden und den Internetplattformen, um den Datenverkehr auf schädliche Daten hin zu filtern. Auch Virenschutzprogramme können auf diese Weise eingesetzt werden und dementsprechend konfiguriert sein.

## Browserverlauf löschen

Wenn Sie sicher sein wollen, dass auf Ihrem Gerät (Computer, Tablet, Smartphone) keine Rückschlüsse auf Ihre Stimmabgabe getätigt werden können, empfehlen wir Ihnen, nach jeder Stimmabgabe den Browsercache zu leeren. Klicken Sie unten auf den Browser, den Sie verwenden, um zur Anleitung zu gelangen, wie man den Browserverlauf löscht.

| [![google-chrome](../icons/google-chrome.png)](del_chrome.md) | [![Microsoft-Edge](../icons/microsoft-edge.png)](del_edge.md) | [![firefox](../icons/firefox.png)](del_firefox.md) | [![apple-safari](../icons/apple-safari.png)](del_safari.md) |
| --------------------------- | ------------------------------------------------------------ | ---------------------------------------- |-------------------------------------------------------------|
| [Google Chrome](del_chrome.md)           | [Edge](del_edge.md)                                         | [Firefox](del_firefox.md)               | [Safari](del_safari.md)                                     |       |
| [Chrome Mobile](del_chromemobile.md) |           [Edge Mobile](del_edgemobile.md)                                                           |                    |   [Safari Mobile](del_safarimobile.md)                                     |            |

## Hashwerte überprüfen (erweiterte Prüfung)

Um sicherzustellen, dass der HTML und Javascript-Code auf dem Gerät des/der Stimmberechtigten die vorhergesehenen Operationen durchführt und die Stimme korrekt verschlüsselt, kann der/die Stimmberechtigte die Integrität des HTML und Javascript Codes prüfen. Dafür kann der/die Stimmberechtigte die Hashwerte der betroffenen HTML und Javascript-Dateien mit den Werten vergleichen, die in den publizierten Protokollen der Kantone und der externen Experten sowie auf dem Portal zur elektronischen Stimmabgabe publiziert sind. Bitte beachten Sie, dass die Integrität des HTML Codes nur manuell geprüft werden kann.

| [![Microsoft-Edge](../icons/microsoft-edge.png)](hash_browser_automatic.md) |
| ------------------------------------------------------------ 
| [automatische Prüfung (SHA384-Hash)](hash_browser_automatic.md)
| [manuelle Prüfung (SHA256-Hash)](hash_browser_manual.md)                                       

## Browsermodus ohne Add-ons nutzen (Inkognito oder Private Modus)

Als weitere Vorsichtsmassnahme empfehlen wir Ihnen die Nutzung eines Browsermodus, der Browser-Add-ons standardmässig unterbindet. Bei Chrome oder Microsoft Edge ist dies der Inkognito-Modus, beim Internet Explorer der InPrivate-Modus und im Safari der private Surfmodus. Sie können auch manuell die Add-ons und Browser-Erweiterungen prüfen und gegebenenfalls deaktivieren.

**Bitte schalten Sie die automatische Übersetzungsfunktion Ihres Browsers aus, um sicherzustellen, dass die Inhalte im Wahl- und Abstimmungsportal korrekt dargestellt werden.**


| [![google-chrome](../icons/google-chrome.png)](incognito_chrome.md) | [![Microsoft-Edge](../icons/microsoft-edge.png)](incognito_edge.md) | [![firefox](../icons/firefox.png)](incognito_firefox.md) | [![apple-safari](../icons/apple-safari.png)](incognito_safari.md) |
| ----------------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| [Google Chrome](incognito_chrome.md)           | [Edge](incognito_edge.md)                                         | [Firefox](incognito_firefox.md)                                   | [Safari](incognito_safari.md)                                    |


## Anleitung zur Anzeige und Synchronisierung der Systemzeit

Um die Stimmabgabe zu ermöglichen, empfehlen wir die Aktivierung der Zeitsynchronisierung auf Ihrem Gerät.

| [![Microsoft-Edge](../icons/time.png)](timecheck.md) |
| ------------------------------------------------------------ 
| [Anleitung zur Anzeige und Synchronisierung der Systemzeit](timecheck.md)