# Google Chrome Mobile

## Supprimer le cache du navigateur sous iOS


1. Cliquez sur les **"trois points"** ”Personnaliser et gérer Google Chrome », marqués ici par la flèche dans la capture d'écran.
2. Cliquez sur le point de menu **Protection des données et sécurité**.
3. Sélectionnez l'option de menu **Effacer les données de navigation**.
4. Sélectionnez dans la liste déroulante au moins la période qui couvre le processus de votre vote électronique. Sélectionnez par exemple l'entrée **"Dernière heure"**.
5) Cochez les cases « Cookies et autres données de site web » et « Images et fichiers mis en cache ».
6. Cliquez sur le bouton de menu **Effacer les données**.

**Screenshot Étape: 1**

![Screenshot Étape: 1](../img/rules/chrome/de/chrome_mobile_incognito.png)
