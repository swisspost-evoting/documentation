# Consigli di sicurezza

Gli elettori possono verificare autonomamente il corretto svolgimento del voto elettronico eseguendo il processo esplicato nelle istruzioni. In particolare, i codici di verifica generati prima di aver espresso il voto devono essere comparati con il codice di verifica ricevuto mediante i documenti fisici di voto.Per trasmettere il voto è necessario trascrivere sul dispositivo utilizzato il codice di conferma presente nella documentazione elettorale fisica.Anche il codice di finalizzazione che viene mostrato in seguito alla trasmissione del voto deve essere comparato con il codice di finalizzazione presente nei documenti fisici. Questa procedura prende il nome di verificabilità individuale. In questo modo gli elettori possono verificare se il loro voto è stato trasmesso senza variazioni e se è stato registrato correttamente all’interno dell’urna elettronica.

Oltretutto gli elettori possono eseguire diversi tipi di modifiche sul dispositivo utilizzato al fine di accertarsi che il proprio voto non sia stato manipolato.

Di seguito sono descritte le diverse misure.

## Verificare il fingerprint del certificato

Per assicurarvi che il portale su cui vi trovate è quello corretto e ufficiale per il voto elettronico, potete verificare il fingerprint del certificato. In tal modo potrete escludere di trovarvi su un sito manipolato o contraffatto. I certificati vengono impiegati per garantire l’autenticità del server web interessato e codificare il collegamento con il server stesso. Questa procedura consente di distinguere i siti web autentici da quelli falsi anche nei casi i cui appaiano visivamente molto simili o del tutto identici. **Tieni presente che i dispositivi iOS al momento non supportano questa funzionalità.**

| [![google-chrome](../icons/google-chrome.png)](zert_chrome.md) | [![Microsoft-Edge](../icons/microsoft-edge.png)](zert_edge.md) | [![firefox](../icons/firefox.png)](zert_firefox.md) | [![apple-safari](../icons/apple-safari.png)](zert_safari.md) |
| ----------------------------------------- | ------------------------------------------------------------ | ---------------------------------------- |-------------------------------------------------------------|
| [Google Chrome](zert_chrome.md)           | [Edge](zert_edge.md)                                         | [Firefox](zert_firefox.md)               | [Safari](zert_safari.md)                                     |       |

### Cosa devo fare se nel mio browser viene visualizzato un fingerprint errato?

È opportuno interrompere la procedura di votazione e informare l’istanza preposta presso il Cantone (team di assistenza).

Se un fingerprint del certificato della pagina web viene visualizzato in modo errato significa che non è presente un collegamento diretto tra il web browser e la piattaforma di voto elettronico. Nella maggior dei casi questa interruzione non deve essere intesa come un tentativo di abuso, bensì, al contrario, come una misura di protezione. Alcune aziende, infatti, interrompono all’interno delle proprie reti il collegamento tra i computer dei collaboratori e le piattaforme internet in modo da bloccare dati potenzialmente dannosi. Anche i programmi antivirus possono essere utilizzati in questo modo e configurati di conseguenza.

## Cancellare la cronologia delle esplorazioni

Per essere sicuri che dal vostro dispositivo (computer, tablet, smartphone) non sia possibile risalire al voto da voi espresso, vi consigliamo di vuotare la cache del browser dopo ogni procedura di voto. Cliccate in basso sul browser di cui fate uso per accedere alle istruzioni sulla cancellazione della cronologia.

| [![google-chrome](../icons/google-chrome.png)](del_chrome.md) | [![Microsoft-Edge](../icons/microsoft-edge.png)](del_edge.md) | [![firefox](../icons/firefox.png)](del_firefox.md) | [![apple-safari](../icons/apple-safari.png)](del_safari.md) |
| ----------------------------------------- | ------------------------------------------------------------ | ---------------------------------------- |-------------------------------------------------------------|
| [Google Chrome](del_chrome.md)           | [Edge](del_edge.md)                                         | [Firefox](del_firefox.md)               | [Safari](del_safari.md)                                     |       |
| [Chrome Mobile](del_chromemobile.md)              |           [Edge Mobile](del_edgemobile.md)                                                           |                    |   [Safari Mobile](del_safarimobile.md)                                     |            |

## Verificare i valori hash (verifica avanzata)

Per assicurarsi che il codice HTML e JavaScript sul dispositivo dell’elettore / dell’elettrice esegua le operazioni previste e che il voto venga codificato correttamente, l’elettore / l’elettrice può verificare l’integrità del codice JavaScript utilizzato. A tal fine, l’elettore / l’elettrice confronta i valori hash dei file HTML e JavaScript coinvolti con i valori pubblicati nei protocolli dei Cantoni e degli esperti esterni, nonché sul portale di voto elettronico. Si prega di notare che l'integrità del codice HTML può essere verificata solo manualmente.

| [![Microsoft-Edge](../icons/microsoft-edge.png)](hash_browser_automatic.md) |
| ------------------------------------------------------------ |
| [Verificare automatico (SHA384-Hash)](hash_browser_automatic.md)
| [Verificare manuale (SHA256-Hash)](hash_browser_manual.md)   

## Impiego della modalità browser senza add-on (Modalità Incognito o Privata)

Quale altra misura di sicurezza consigliamo di utilizzare una modalità browser che blocca gli add-on per default. In Chrome o Microsoft Edge si tratta della modalità Inkognito, in Internet Explorer della modalità InPrivate e in Safari della modalità di navigazione privata. Potete anche verificare e se necessario disattivare gli add-on e le estensioni del browser anche manualmente.

**Si prega di disattivare la funzione di traduzione automatica del browser per garantire che i contenuti del portale di voto elettronico vengano visualizzati correttamente.**

| [![google-chrome](../icons/google-chrome.png)](incognito_chrome.md) | [![Microsoft-Edge](../icons/microsoft-edge.png)](incognito_edge.md) | [![firefox](../icons/firefox.png)](incognito_firefox.md) | [![apple-safari](../icons/apple-safari.png)](incognito_safari.md) |
| ----------------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| [Google Chrome](incognito_chrome.md)           | [Edge](incognito_edge.md)                                         | [Firefox](incognito_firefox.md)                                   | [Safari](incognito_safari.md)                                    |

## Guida per visualizzare e sincronizzare l'orario di sistema

Per consentire il voto, ti raccomandiamo di attivare la sincronizzazione dell'orario sul tuo dispositivo. 

| [![Microsoft-Edge](../icons/time.png)](timecheck.md) |
| ------------------------------------------------------------ |
| [Guida per visualizzare e sincronizzare l'orario di sistema](timecheck.md)