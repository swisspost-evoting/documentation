# Instructions on how to verify the HTML and JavaScript files of the e-voting portal in the browser

## Verifying the hash value of the HTML and JavaScript files in the browser

- If you would like to check that the HTML and JavaScript files used by the official e-voting portal have not been altered, you can verify the relevant hash values.
- If you do this before you cast your e-vote, you can rule out the possibility of the HTML and JavaScript files being manipulated or falsified (replaced).
- The following description is based on the JavaScript file **main.js**, though it may also be used for other JavaScript files. To check the HTML file, you can display the page source in the browser by right-clicking and directly proceed to step 5.
- Please ensure that no HTML and JavaScript files other than those listed in the protocol are loaded **(5 JavaScript files) and one index.html file**.

### Procedure for the manual calculation of the hash values (SHA256-Hash)

1. Search for the **SHA256 hash** of the **main.js** JavaScript files in the published protocols of the cantons and the experts. You can find the link to the minutes on the voting portal under the heading **Trusted Build**.

2. Start the voting process and then open the browser’s **Developer tools** in one of the following ways:

- Press **F12** on the keyboard or
- Press **Ctrl+Shift+I** on the keyboard
- Click on the ... icon, and then on **More tools** , followed by **Developer tools**

3. Click the tab **Sources** and then select the source **main.js**:

4. Deactivate the pretty print function (if the icon is blue, then it is active).

![img](../img/rules/general/prettyprint.png)

5. Copy the contents of the file to any SHA256 hash conversion page and let it calculate the **SHA256 hash**.

6. Compare the **hash value** displayed with the hash value displayed in the published protocols. Please check if the calculated value is displayed in hexadecimal or Base64 format. Please note that the manually calculated SHA–256 is usually displayed in [Base16 format](https://www.rfc-editor.org/rfc/rfc4648#section-8), while the `integrity` tag displays the SHA–384 hash value in [Base64 format](https://www.rfc-editor.org/rfc/rfc4648#section-4) in accordance with the [W3C recommendation](https://www.w3.org/TR/SRI/). You can also check the hash values displayed on the election and voting portal.

**if both the strings are identical, the HTML or JavaScript file has not been altered.**

