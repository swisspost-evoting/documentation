---
layout: article
title: Infrastructure Whitepaper of the Swiss Post Voting System
author: © 2024 Swiss Post Ltd.
date: "2024-03-06"

toc-own-page: true
titlepage: true
titlepage-rule-color: "ffcc00"
titlepage-text-color: "ffffff"
footer-left: "Swiss Post"
titlepage-background: ".gitlab/media/swisspostbackground.png"
...

# Infrastructure Whitepaper of the Swiss Post Voting System

## Introduction

The infrastructure whitepaper describes the e-voting infrastructure with all implemented security aspects. This includes information on the data centres, the structure and use of the infrastructure and databases and the various security measures.

The Swiss Post Voting System enables eligible voters to participate electronically in votes and elections. The vote is cast via the web browser on a computer, smartphone or tablet. The various software components and the system architecture are explained in the [system specification and system architecture documents](../System).

## IT infrastructure

### Data centres and business continuity

Swiss Post has two geographically separate data centres. All E-Voting Systems are redundantly located in both data centres. If the primary data centre DC1 fails, the secondary data centre DC2 takes over the services. The following graphic illustrates the site redundancy.

![Graphic of the data centres (DC)](../.gitlab/media/infrastructure_whitepaper/image2.png)

The infrastructure is designed to be disaster-proof. The following features distinguish our data centres:

Features

- FINMA-compliant, TÜV "Dual Site Level 3"-certified

- The operator is ISO 27001- and ISO 22301-certified

- Complete redundancies of critical supply systems

- No single point of failure

- Strongly authenticated access control

- Uninterruptible power supply


In addition to the geo-redundancy, DC1 has a room redundancy with two completely independent installations, each with its servers, power supplies etc. and both fire protection separated from each other. It ensures business continuity at all times. The following graphic illustrates the room redundancy:

![Graphic of the business continuity at the data centres](../.gitlab/media/infrastructure_whitepaper/image3.png)

Key:

- DC: data centre

### E-voting infrastructure

The e-voting infrastructure is set up for use within the cantons. Each canton has its own e-voting environment, which is logically completely separate from the environments of the other cantons. The following graphic shows the structure:

![Graphic of the e-voting infrastructure](../.gitlab/media/infrastructure_whitepaper/image4.png)

Key:

- FEV: e-voting service front end

- BEV: e-voting service back end

- RP: reverse proxy

- CCn: control component

- DB: database

- ODA: Oracle database application

The following chapters explain the access layer and different system environments.

### Access layer
The access layer manages access to the e-voting infrastructure using a reverse proxy infrastructure. Swiss Post's reverse proxies run on physical hardware. There are two physical production servers per data centre.

The reverse proxy functionality availability is ensured by dedicated server with several instances of the reverse proxy software. For each hardware server, there is one voter reverse proxy, one voter-SDM reverse proxy per canton. The instances are logically separated from each other. They run as different users with different certificates and separate IP addresses on specially hardened servers under a high-security operating system.

An open-source security framework is used on the reverse proxies. The software related is operated with two different sets of rules. The voting reverse proxy must be accessible to worldwide clients. The access cannot be authenticated using a client certificate and therefore a custom set of proxy rules was developed, that allows only a short, predefined list of addresses to access to the server.

The e-voting set-up per canton comprises of separate network zones, the public voter network and the administration network zone. The two zones are completely separate, cross-connections are not permitted and are made impossible by firewalls. The voter zone is used for the actual ballot for the citizens to vote. The administration network zone is used to create an election event with the Secure Data Manager, SDM.

### Integration and production cluster

The integration and production systems are set up within an Kubernetes cluster. The cluster is in turn deployed on virtualised servers. With the use of this technology, various factors can be ensured:

- The [Kubernetes](https://kubernetes.io/de/docs/home/) Cluster is an open-source system for automating the deployment, scaling and management of container applications in highly available infrastructure.

- By using container technology and deployment on the fly, high availability can be ensured and downtime reduced.

- Continues Deployment is an essential part of scalability.

- Resource distribution and resource utilisation can thus be optimally controlled for the various loads.

- If a worker node fails, the load is automatically distributed to the remaining nodes, thus ensuring that the application remains available.

- All Control Components are based on Baremetal Server and excluded from the cluster.

The following diagram shows a schematic overview of the structure:

![Graphic of the integration and production cluster](../.gitlab/media/infrastructure_whitepaper/image6.png)

Key:

- SNI = server name identification

- Worker Node = virtual machine

Security is an essential factor for the e-voting infrastructure. Firewall demarcations within the cluster, the worker nodes and the cantonal instances are ensured by automated IP-Tables configurations. Using special forensic software further increases security. Communication outside and inside the cluster uses exclusively encrypted connection paths via TLS.


Several Pods on different Kubernetes worker nodes are provided for each canton. Each worker node is a virtualised server based on VMWare. The scaling of the individual instances takes place according to the load or through the set CPU / memory of the lower and upper limits. If the upper limits are reached, a new POD is automatically started to distribute the load as efficiently as possible. If the cluster management determines that the load is again within the specified values, Pods that are no longer required are shut down again, based on Elasticity concept. For each canton, two Pods on each Data Center of the voter front and back end and a message service cluster consisting of three Message-Broker Pods are created as a basic set-up.

Further information on Kubernetes and Rancher:

- [Kubernetes](https://kubernetes.io/de/docs/home/) homepage

- [Rancher](https://rancher.com/docs/rancher/v2.x/en/overview/architecture/) architecture

Each cantonal e-voting instance is managed as a Rancher project with its own unique Kubernetes namespace. Within the cluster, the individual projects are shielded from each other and from outside to inside by firewall rules. This ensures that all cantons are shielded from each other and that access is possible only within each canton's own Rancher cluster project.

### Database infrastructure

The e-voting database infrastructure employs dedicated systems. The data must always be consistent and no data may be lost at any time. Therefore, the systems have "triple mirroring" and "zero data loss" to ensure consistency and prevent loss of data.

## E-voting security

Swiss Post operates an e-voting system that guarantees individual and universal verifiability and voting secrecy in accordance with [the applicable VEleS (Ordinance of the Federal Chancellery on Electronic Voting, SR 161.116](https://www.fedlex.admin.ch/eli/cc/2022/336/en)). The infrastructure of the Swiss Post Voting System ensures the safe and reliable transport and storage of the encrypted votes by additional security measures. The following diagram clearly shows the security-relevant aspects that have been implemented for Swiss Post's e-voting solution:

![Graphic of e-voting security](../.gitlab/media/infrastructure_whitepaper/image9.png)

Key:

- SSL= Secure Sockets Layer (SSL), network protocol

- Voter front end = web-based interface, execution of the vote

- Voter back end = Voting Server

- SDM = Secure Data Manager (SDM) \
    Locally installed program to set up an election event.

## Safety measures

The safety measures are described in the following chapters.

### Individual and universal verifiability, effective authentication, and voting secrecy

The Swiss Post Voting System employs a cryptographic protocol guaranteeing individual and universal verifiability, effective authentication, and voting secrecy. 
More detailed information can be found in the [Protocol of the Swiss Post Voting System](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/tree/master/Protocol).

### Control components


The Swiss Post Voting System is designed to decentralize critical operations, notably the decryption of votes, across a group of "control components." 
These components are diversified in terms of operating systems, hardware configurations, and operating teams. 
This diversification makes it exceedingly challenging for potential adversaries to compromise the integrity of the entire group of control components.

The control components are an essential part of the security measures of the e-voting service. There are 12 Blade Servers with four different operating systems, three for each control component with fourfold redundancy and, to further increase the security against attacks, they run on four different operating systems and are operated by four different operating teams.

The control components are subject to separate security measures. In the trust model of universal verifiability, only the control components in their combination are trusted. In addition to the security elements of the e-voting platform that have already been mentioned, additional precautions have been taken to increase security. These support the trust model.

The control components are operated independently so that a possible successful attack on one component cannot affect another. This ensures that the trustworthiness of a group of control components remains guaranteed.

The operation and monitoring of the control components are the responsibility of different persons. Specific hardware components and the operating systems of the control components differ. The control components are connected to different networks. They are accessible only to persons who are responsible for the operation and monitoring of a specific control component. Access attempts are detected and reported to the person responsible for the corresponding control components.

All control components are based on dedicated and disaster-proof hardware. Each control component group (also called CC group) has its own hardware. The use of the four control component groups aims to ensure that the cryptographic operations are checked by several systems that use different technology (e.g. different operating systems). The first operating part is performed in parallel and the results are valid only if all four results are identical. The second operations part such as mixing are performed sequentially, and the final result is valid only if all four partial results are correct.

![Graphic of the control components](../.gitlab/media/infrastructure_whitepaper/image11.png)

The control components consist exclusively of physical servers (bare-metal approach). Further security measures are then taken in the course of configuring the servers. Furthermore, the control components differ physically from each other. This means that different server models with different processor architectures are in use.

Each control component has instances deployed in both data centers. The 12 servers are distributed in 2 units each to the data centres DC1 and DC2 and connected by redundant load balancers. These load balancers primarily serve to ensure high availability. If a control component instance (Server, Loadbalancer) in one data centre fails, the other control component instance, located in the other data centre, takes over the service. Even in the event of a total failure of a control component in a data centre, this concept ensures smooth operation. Furthermore, it is ensured that no control component server of the same group is physically installed in the same rack, and the racks within the high-security data centre are also locked.

For the e-voting control components, a separate network area was set up in a designated secure zone at Swiss Post. This area in turn contains four different zones (1-4), which are separated/protected from each other by means of a firewall. The e-voting area, in turn, is separated/protected from the other areas located in the superordinate zone by means of a firewall. Each additional zone is in turn separated by a firewall.

Additional security measures are in place for the database servers. Each control component owns its own database that runs on their exclusive high availability, physically separated, bare metal servers. The same security measures apply to the database servers as to the control components application servers, such as different operating systems, segregation of duty and the 4 eyes principle and monitoring.

### Access layer / reverse proxies

A important part of the security measures focuses on the access layer with its reverse proxies. This is described in chapter [Access layer](#access-layer). Essentially, it is about enforcing mandatory access control at both operating systems and the application level.

### Firewalls, zones and areas

The zone and area concept separates the servers from each other in terms of network technology. This means that the access layer, e-voting frontend/backend and database are in different network zones and cannot see each other. Each zone and area is protected by firewalls. The connection is made by means of regulated and defined firewall rules. The firewall rules define which traffic is allowed and which is prohibited through a firewall. The access layer is physically separated from the rest of the e-voting infrastructure.

### SDM access

The SDM (Secure Data Manager), of which one instance is used to set up the election event and another to tally the election results, connects to the admin reverse proxy and the voter SDM reverse proxy. There is a mutual TLS certificate on the SDM. This is validated by the reverse proxies and used for authentication. This means that only this client can call up the various Voting Admin URLs. The SDM client is always used based on the multi-eye principle and is stored securely when not in use.

### High-security operating system

The infrastructure is built on an open-source high-security operating system that supports the access control mechanism. All servers, except those of the control components (see chapter [E-voting Infrastructure](#e-voting-infrastructure)), for the e-voting platform (reverse proxies and the e-voting servers), use this operating system.

This high-security operating system means that an e-voting server process runs in its own context and is completely encapsulated. This means that the process can access only the designated resources of the system. All other resources are not available to it (mandatory access control).

### Firewall (Iptable)

In addition to Swiss Post's own physical network firewalls, firewall software is also used at the operating system level on the entire e-voting platform to provide additional access protection. Only access to the necessary system management ports and from defined web server IP addresses is permitted.

### Authentication at SSL/TLS level

The various servers communicate in encrypted form. They always authenticate each other by means of certificates. This ensures complete end-to-end encryption (from the recording to the counting of votes) for e-voting.

### Integrity monitoring

Swiss Post uses Grafana and Prometeus solution for e-voting for integrity monitoring of the operating system and as an IDS (intrusion detection system) on the entire platform.

### JS response check

The reverse proxy validates the JavaScript files that are sent to a client (voter). In other words, the reverse proxy checks the hashes of the files in the HTTPS response body, which are delivered by the back-end systems. If there is a deviation from the configured hash value, it refuses to deliver a possibly manipulated file. This is a control measure that ensures the correct JavaScript-based encryption of the ballots on the client.

### Air gap principle

The canton operates several workstations (clients) with the SDM software. These are needed for the configuration and tally phase of an election event. A fundamental distinction is made here between online and offline devices. The offline device does not have access to a network at any time. Data is transmitted exclusively via encrypted data carriers.

The recommended safety measures are described in the document "[Recommendation Safety Measures](../Operations)".

### Four-eyes principle

The four-eyes principle controls administrative access to the complete e-voting infrastructure. If Swiss Post system administrators want to access an e-voting component, they need a token number or a group membership, which they receive from another person from another department after identity and justification checks. This token number is valid only once and expires as soon as the administrator logs off again. The group membership is removed directly after the work is completed and the access expires as soon as the administrator logs off.

### E-voting monitoring

The infrastructure components are monitored according to a standardised and ISO-certified process. Alarms are carried out according to defined threshold values via SMS and/or e-mail alerts.

In addition to this monitoring, a so-called voter monitoring was set up for e-voting. The e-voting application generates specific logs with events that can be assigned to individual phases in the voting process. Thus, received and completely anonymised votes can be observed, searched, filtered, statistically analysed and graphically evaluated in real-time during a ballot. This monitoring primarily serves to control the orderly process of electronic voting. Critical conditions or anomalies during a ballot trigger an alert that is transmitted by SMS to the defined offices.

### E-voting deployment process

The deployment process describes how a new release is installed on the e-voting platform. Please note that the integrity of the created release is checked by means of a checksum and that the deployment is possible only with the principle of dual control. Each deployment is recorded in Swiss Post\'s change management tool, tracked and approved after completion of the deployment. Each release is created and published via the trusted build. In the Trusted Build process, the sources from GitLab, which are publicly accessible, are used. In a further step, it is checked whether these sources correspond to those in Swiss Post IT source management. This serves to prove that the sources in GitLab have not been falsified. If this is not the case, the artifacts are created and then the Docker images are created in turn from them. All these processes are monitored by an independent commission. Further information on the Trusted Build can be found in the document "[Trusted Build of the Swiss Post Voting System](../Operations)".

