
| Artefact     | Version    | Checksum    |
| --------|---------|---------|
| **secure-data-manager-package-1.3.4.0.zip**  |  1.3.4.0  |  6db07137ec01ce724d3c80109186e95bf472eabf4feb55647c097b6f462c899e |
| **voter-portal-1.3.4.0.zip**  |  1.3.4.0  |  a569742ecbc452411777d8541760be500f9d705c797ff64aa7ea36062e4547b0 |
|    --> index.html  |  1.3.4.0  |  f9b36103452e1035dedc667bbdccbeb1f1deb5e8bfecd261cf6d298370deb8ab |
|    --> crypto.ov-api.js  |  1.3.4.0  |  2faf9fae8d6bf75cc1ca06aa9e4f5f2ae4f220420e9d047d884b3cac7bdb26a3 |
|    --> crypto.ov-api.js  |  1.3.4.0  |  sha384-7G9+mPIpCYXzspoKVxrbB9gypA8LQN3AcdpKeGkl0JF/fMDXMpwPEuJuy43BX6Eb |
|    --> crypto.ov-worker.js  |  1.3.4.0  |  9003a5a2dff12259282d571fc17084fed8c16bab40490510a71bed9309ff7370 |
|    --> crypto.ov-worker.js  |  1.3.4.0  |  sha384-QOSlTdVQ16wedQrA4QJYcUx3mK/UdyZ1yMF4IJW/XSDxero/1BqlvPky8RxA0GVY |
|    --> main.js  |  1.3.4.0  |  d7564715dac28ecf972c1bc81e5e8980c7e94945b935445433c003def4e5d0f6 |
|    --> main.js  |  1.3.4.0  |  sha384-4s2RRwKzdg4h375qYVuBDzlwTwV0CKWEEEbEZ3LUmzaZ6qGLz+3XiLumaK4bSPGk |
|    --> polyfills.js  |  1.3.4.0  |  34dfb6e7c7123409169c1b505914dcf3a93b020cddf6d920af248aba9325b99a |
|    --> polyfills.js  |  1.3.4.0  |  sha384-ADkau9TRfRJgjPSietYkJtX6HXQO0rHfv3Grxl4mrWOXJStCoIp3NYVehF+jV2tb |
|    --> runtime.js  |  1.3.4.0  |  12aabd163c442aae3e55119869e24e35654310fb3de1ced5aaf68f6400956ef1 |
|    --> runtime.js  |  1.3.4.0  |  sha384-dl5N3uRQoV0Gvcn8R0Iwi5XWok4Hru2MaccA0GUVXpz+8KUybTBAJ88CJCSpQGiR |
| **voting-server-1.3.4.0-runnable.jar**  |  1.3.4.0  |  f437c3dbd3dd69a3addd266297600603c37e70a06cc1c756984fdbed45578ae4 |
| **control-component-runnable.jar**  |  1.3.4.0  |  470ea62f1db85f6cf1e7e992f6568b2de79a56bcf0f99bf738f06180c2a327f6 |
| **config-cryptographic-parameters-tool-1.3.4.0.jar**  |  1.3.4.0  |  4ba919acebd943820c3de714de709b63d3d30475eb4ff62956bbfb7f94b2a116 |
| **xml-signature-1.3.4.0.jar**  |  1.3.4.0  |  1006d8befb1545d48e7d349e01a463fb334b53e435a79f72aa31c24955b9a158 |