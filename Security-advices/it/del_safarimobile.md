# Safari Mobile

## Cancellare la cache del browser su iOS


1. Accedere a **Impostazioni** sull'iPhone e selezionare **Safari**.
2. Selezionare **“Cronologia” e “Cancella dati sito web ”**.
3. Confermare il messaggio cliccando su **"Cancella dati e cronologia”**.

**Screenshot Passo: 1**

![Screenshot Passo: 1](../img/rules/safari/mobile/it/1.png)

**Screenshot Passo: 2**

![Screenshot Passo: 2](../img/rules/safari/mobile/it/2.png)

**Screenshot Passo: 3**

![Screenshot Passo: 3](../img/rules/safari/mobile/it/3.png)
