# Anleitung zur automatischen Prüfung der JavaScript-Dateien des E-Voting Portals im Browser

## Prüfung des Hash-Wertes der JavaScript-Dateien im Browser

- Wenn Sie überprüfen möchten, ob sich die vom offiziellen E-Voting-Portal verwendeten JavaScript-Dateien in unverändertem Zustand befinden, können Sie die jeweiligen Hash-Werte überprüfen. Bitte beachten Sie, dass nur der JavaScript automatisch geprüft werden kann, aber nicht die Integrität der HTML Datei.
- Tun Sie dies vor der elektronischen Stimmabgabe, können Sie ausschliessen, dass die JavaScript-Dateien manipuliert oder gefälscht (ersetzt) worden sind.
- Die folgende Beschreibung erfolgt anhand der JavaScript-Datei **main.js**, kann aber auch für andere JavaScript-Dateien verwendet werden.
- Bitte stellen Sie sicher, dass ausser den im Protokoll aufgeführten JavaScript Dateien **(5 JavaScript-Dateien)**, keine anderen JavaScript Dateien geladen werden.

### Vorgehen mit automatischer Berechnung der Hashwerte durch den Browser (SHA384-Hash)

1. Suchen Sie in den publizierten Protokollen der Kantone und der Experten den **SHA384-Hash** der JavaScript-Dateien **main.js**. Sie finden den Link zu den Protokollen auf dem Abstimmungsportal unter der Überschrift **Trusted Build**.

2. Öffnen Sie mit einem Rechtsklick, dann auf den Menüpunkt **"Seitenquelltext anzeigen"** im Abstimmungsportal den Quelltext der HTML Seite.
- Am Ende der Datei index.html finden Sie die relevanten ```<script>``` Tags mit den Hash-Werten als "integrity" attribut. Das integrity Attribut instruiert den Browser zu prüfen, dass der Hashwert im Integrity Tag mit dem Hashwert der Javascript Datei übereinstimmt.

Beispiel:

```<script src="main.js" type="module" crossorigin="anonymous" integrity="sha384-QSlsWXjv0VwQ8BJSA9Z98koIF5pTfCGsP0fuhZ48u3U0deu+eecG45aHG2lqtHH0"></script>```

3. Vergleichen Sie den angezeigten **SHA-384-Hash-Wert** im integrity tag mit dem Hash-Wert der in den publizierten Protokollen angezeigt wird. Bitte beachten Sie, dass gemäss der [W3C-Empfehlung](https://www.w3.org/TR/SRI/) der `integrity`-Tag den SHA-384-Hashwert im [Base64-Format](https://www.rfc-editor.org/rfc/rfc4648#section-4) anzeigt, während der manuell berechnete SHA-256 üblicherweise im [Base16-Format](https://www.rfc-editor.org/rfc/rfc4648#section-8) angezeigt wird. Zusätzlich können sie auch die Hashwerte überprüfen, die auf dem Wahl- und Abstimmungsportal angezeigt sind.

**Sind die beide Zeichenfolgen identisch, ist die JavaScript-Datei nicht verändert worden.**