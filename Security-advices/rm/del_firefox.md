# Firefox

## Stizzar la cronologia dal navigatur en Mozilla Firefox



1. Cliccar sin la **"trais lingias orizontalas"** sisum a dretga, marcà cun in paliet en il maletg.
2. Tscherni il punct da menu **Chronica**.
3. Cliccar sin il punct da menu **"Stizzar la cronica la pli nova"**. (cumbinaziun da tasta: Strg + Umschalt + Entf)
4. Tscherni en la glista da Dropdown almain quella perioda che cumpiglia il proceder da Vossa votaziun electronica. Tscherni per exempel l'inscripziun **"L'ultima ura"**.
5. Cliccar sin la tasta da menu **"Ussa stizzar"**

**Screenshot Pass: 1**

![Screenshot Pass: 1](../img/rules/firefox/de/1.png)
