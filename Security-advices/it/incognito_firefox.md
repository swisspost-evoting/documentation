# Firefox

## Abilitare la modalità privata in Firefox


1. Fare un clic sulle **tre linee orizzontali** in alto a destra, contrassegnate da una freccia nell'immagine.
2. Fare un clic su **Nuova finestra privata**.
3. Continuare a lavorare nella nuova **finestra privata**, riconoscibile dallo **sfondo scuro**.

**Screenshot Passo: 1**

![Screenshot Passo: 1](../img/rules/firefox/de/1.png)

**Screenshot Passo: 3**

![Screenshot Passo: 3](../img/rules/firefox/de/incognito_firefox.png)
<br>