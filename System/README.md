# What is the content of this folder?

|Document|Summary|Download|
|:-------|:------|:------:|
|[Architecture of the Swiss Post Voting System](SwissPost_Voting_System_architecture_document.pdf)|Architecture documentation of the e-voting system based on the arc42 architecture documentation style. In case this is unfamiliar the arc42 site provides a concise overview and examples which are worth taking a moment to review. <br> An overview of all major changes within the published releases is available [here](CHANGELOG.md). | <a href="https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/raw/master/System/SwissPost_Voting_System_architecture_document.pdf?inline=false"><img src="../.gitlab/media/icons/button_download-pdf.png" alt="Download document as PDF" width="186" height="30"></a> |
|[Verifier of the Swiss Post Voting System](Verifier_Specification.pdf)| The Swiss Post Voting System requires a verification software—the verifier—to verify the cryptographic evidence. The specification and development of the verifier goes hand in hand with the Swiss Post Voting System, and the verifier challenges, as well as extensively tests a protocol run. <br> An overview of all major changes within the published releases is available [here](CHANGELOG.md)| <a href="https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/raw/master/System/Verifier_Specification.pdf?inline=false"><img src="../.gitlab/media/icons/button_download-pdf.png" alt="Download document as PDF" width="186" height="30"></a> |
|[Specification of the Swiss Post Voting System](System_Specification.pdf)| The system specification provides a detailed specification of the cryptographic protocol — from the configuration phase to the voting phase to the tally phase — and includes pseudo-code representations of most algorithms. <br> An overview of all major changes within the published releases is available [here](CHANGELOG.md)| <a href="https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/raw/master/System/System_Specification.pdf?inline=false"><img src="../.gitlab/media/icons/button_download-pdf.png" alt="Download document as PDF" width="186" height="30"></a> |

# Specification of the Swiss Post Voting System

## What is the content of this document?
The system specification provides a detailed specification of the cryptographic protocol — from the configuration phase to the voting phase to the tally phase — and includes pseudo-code representations of most algorithms. The document has the following target audiences:
- developers implementing the Swiss Post Voting System. 
- reviewers checking that the system specification matches the [computational proof](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/tree/master/Protocol). 
- reviewers checking that the implementation (source code) matches the system specification. 

## Why is a detailed specification of the cryptographic protocol important?

[Haenni et al](https://arbor.bfh.ch/13834/) stressed the importance of a precise system specification—using mathematical formalism and pseudo-code descriptions of algorithms—to reduce implementation errors and increase auditability of an e-voting system.

The system specification details all relevant cryptographic operations of the cryptographic protocol and communications between protocol participants. The pseudo-code representations are sufficiently precise to leave no room for interpretation while being independent of any programming language.

Moreover, the system specification links the [computational proof](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/tree/master/Protocol) which formally proves verifiability and vote secrecy to the actual source code.

## Changelog

An overview of all major changes within the published documents is available [here](CHANGELOG.md).

## Changes since publication 2019

We have improved the system specification in multiple ways since the initial publication of 2019:

* Completeness: We provide explicit algorithms for representing and converting basic data types.
* Clarity: Pseudo-code algorithms detail the domain of input and output algorithms and provide a mathematically precise description of the operations.
* Consistency: Method and variable names align more closely to the [computational proof](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/tree/master/Protocol).
* Structure: The specification describes low-level algorithms separately and references them in high-level algorithms; therefore avoiding repetition.
* Readability: Sequence diagrams and overview tables increase the specificiation's readability.
* Separation of concerns: Each chapter focuses on a main aspect of the protocol. For instance, we moved the description on channel security (using digital signatures) to a separate chapter.

## Future Work

In future versions, we plan to address the following issue:

* Further performance optimizations in the setup component.

## Limitations

We would like to mention the following limitations of the system specification:

* The system specification does **not** provide a formal proof of verifiability and vote secrecy. Please check the [computational proof document](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/tree/master/Protocol) if you are looking for a security analysis.
* The system specification is bound to the [same limitations](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/tree/master/Protocol#limitations) than the computational proof regarding quantum computers, a trustworthy voting client for vote secrecy, and a trustworthy setup and printing component.
* The system specification does **not** describe which programming language and frameworks implement the protocol, the principles and design patterns that the source code follows, and the technical and organizational constraints that the solution is bound to. These aspects will be covered in the architecture documentation.
* The system specification does **not** detail the verifier's algorithms, which will be described in the verifier specification.
