
| Artefact     | Version    | Checksum    |
| --------|---------|---------|
| **control-components-runnable.jar**  |  1.2.1.0  |  bcf49ee3dac85c476bbf35e4b80cab63b15613221585b7b36a5a06dd3361a560 |
| **secure-data-manager-package-1.2.1.0.zip**  |  1.2.1.0  |  df9870e9bfb5cf2f0afcf3937d797fba477316f47b42dd7c1aa9477ded08c8fb |
| **voter-portal-1.2.1.0.zip**  |  1.2.1.0  |  5d3726ab1ad086b3eaacf2592eba9755b6a0c52ea7db7abd25609706a4ef9b76 |
|    --> crypto.ov-api.min.js  |  1.2.1.0  |  d3c9229b6840b86af0b51c8a9e888f84a1228760ca7ebc7213e228f231b0f710 |
|    --> main.js  |  1.2.1.0  |  5e532a38edbbccd0cb1993704782cf9c535ce831e410464201bc27b259b5e8cf |
|    --> polyfills.js  |  1.2.1.0  |  4b448effec426a2c384bd53d1c469bef63618a2fa9eab4a851ecb0e230cae675 |
|    --> runtime.js  |  1.2.1.0  |  567268796e7d872676c3a79ee5d3eb0fbddd7c2e8fc28439ad64e06bf948678c |
| **ag-ws-rest.war**  |  1.2.1.0  |  72cd0bed5e26188e14c4de510a636872e815e3e823a112ff705a1ce9ce96e85c |
| **au-ws-rest.war**  |  1.2.1.0  |  217b3f7a7f1c45870093ec2712571f4a0351bc312217627acfb0136d5d63fd2b |
| **cr-ws-rest.war**  |  1.2.1.0  |  5196e67d86c451d2b7da77b3b0dd041b1e412992efc59609fbc4cd4be5b69667 |
| **ei-ws-rest.war**  |  1.2.1.0  |  2e01b0aef94466f53fa8196f2185ee898de68250016387190b63e5b4f254d839 |
| **ea-ws-rest.war**  |  1.2.1.0  |  238743faea649d9bff2a55de925040a599972d54897929201f5956dcc5b109f3 |
| **message-broker-orchestrator-1.2.1.0.jar**  |  1.2.1.0  |  5535fff2c0b35855d102343359aca56d760f9f726a4856050c95ff9483efc9cb |
| **vv-ws-rest.war**  |  1.2.1.0  |  9196783cc1b331e2811d206d0eb9171b9671ceb44ee41b4358946038657cd5f7 |
| **vm-ws-rest.war**  |  1.2.1.0  |  8247240a69e14ad53589419a4333719b6fff8eb2c26fd4dd8ddac9ba359f9c8a |
| **vw-ws-rest.war**  |  1.2.1.0  |  63f6a7dd8924cdd4a63f5d3dc6ae0d6738fde4226f874230ef762c59daf8caae |