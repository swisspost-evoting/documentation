# What is the content of this folder?

| Document | Content | Download |
| :------- |:--------|:--------:|
| [Electoral Model](ElectoralModel.md) | A quick start and an overview of the electoral models supported by the Swiss Post e-voting system. | <a href="https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/jobs/artifacts/master/raw/ElectoralModel.pdf?job=CONVERT"><img src="../.gitlab/media/icons/button_download-pdf.png" alt="Download document as PDF" width="186" height="30"></a> |
| [Software development process for the Swiss Post voting system](Software%20development%20process%20of%20the%20Swiss%20Post%20voting%20system.md) | This document describes the software development process. Among other things, it provides information on the agile approach and the tools used, shows the quality aspects of software development and gives an overview of software specification for mathematical algorithms. | <a href="https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/jobs/artifacts/master/raw/Software%20development%20process%20of%20the%20Swiss%20Post%20voting%20system.pdf?job=CONVERT"><img src="../.gitlab/media/icons/button_download-pdf.png" alt="Download document as PDF" width="186" height="30"></a> |
| [Security Whitepaper of the Swiss Post Voting System](Security%20Whitepaper%20of%20the%20Swiss%20Post%20Voting%20System.md) | This document describes the security features and mechanisms of the e-voting system.  | <a href="https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/jobs/artifacts/master/raw/Security%20Whitepaper%20of%20the%20Swiss%20Post%20Voting%20System.pdf?job=CONVERT"><img src="../.gitlab/media/icons/button_download-pdf.png" alt="Download document as PDF" width="186" height="30"></a> |
| [Product Roadmap](Product_Roadmap.md) | The roadmap sets out the dates for the availability of releases and documents that are planned to be made available. The planning is currently made up to the production release and will be extended later. | - |
| [ISO/IEC 27001:2022 Certification](ISO) | This folder contains documents related to the ISO/IEC 27001:2022 certification of Swiss Post. | - |



