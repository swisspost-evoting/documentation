# Google Chrome Mobile

## Browserverlauf löschen in Google Chrome Mobile unter iOS



1. Klicken Sie auf die **"drei Punkte"** "Google Chrome anpassen und verwalten", hier im Screenshot mit dem Pfeil markiert.
2. Klicken Sie auf den Menüpunkt **Datenschutz und Sicherheit**.
3. Wählen Sie den Menüpunkt **Browserdaten löschen**. 
4. Wählen Sie in der Dropdown-Liste mindestens den Zeitraum, der den Vorgang Ihrer elektronischen Stimmabgabe umfasst. Wählen Sie zum Beispiel den Eintrag **"Letzte Stunde"**.
5. Wählen Sie die Kontrollkästchen "Cookies und andere Websitedaten" sowie "Zwischengespeicherte Bilder und Dateien" aus.
6. Klicken Sie auf die Menüschaltfläche **Daten löschen**.

**Screenshot Schritt: 1**

![Screenshot Schritt: 1](../img/rules/chrome/de/chrome_mobile_incognito.png)


