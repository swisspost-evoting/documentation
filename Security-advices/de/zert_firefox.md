# Firefox

## Überprüfung Zertifikat


1. Klicken Sie auf das **Schloss-Symbol** neben der Adresszeile, hier im Screenshot mit dem Pfeil markiert.
2. Klicken Sie anschliessend auf **"Verbindung sicher"**.
2. Klicken Sie auf **"Weitere Informationen"**.
3. Klicken Sie unter dem Menü **Sicherheit** auf **"Zertifikat anzeigen"**.
4. Vergleichen Sie unter dem Absatz **Fingerabdrücke** den SHA-256 Wert, mit demjenigen auf Ihrem Stimmrechtsausweis. Er **muss identisch sein**.

**Screenshot Schritt: 1**

![Screenshot Schritt: 1](../img/rules/edge/de/edge_zert1.PNG)


**Hinweis:** Die Webadresse setzt sich stets wie folgt zusammen: ct.evoting.ch, wobei «ct» für das Kürzel des jeweiligen Kantons steht.