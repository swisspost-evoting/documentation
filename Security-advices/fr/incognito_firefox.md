# Firefox

## Activer le mode privé dans Firefox


1. Cliquez sur les **trois lignes horizontales** tout en haut à droite, marquées ici dans la capture d'écran par une flèche.
2. Cliquez sur **nouvelle fenêtre privée**.
3. Continuez à travailler dans la nouvelle **fenêtre privée**, que vous reconnaissez à son **fond sombre**.

**Screenshot Étape: 1**

![Screenshot Étape: 1](../img/rules/firefox/de/1.png)

**Screenshot Étape: 3**

![Screenshot Étape: 3](../img/rules/firefox/de/incognito_firefox.png)
<br>