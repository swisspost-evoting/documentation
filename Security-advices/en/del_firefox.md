# Firefox

## Deleting browsing history in Firefox


1. Click on the **"three horizontal lines"** at the top right, **marked with an arrow in the screenshot.**
2. Select the menu item **Chronicle**.
3. Click on the menu item **"Delete latest history**. (Key combination: Ctrl + Shift + Del)
4. In the drop-down list, select at least the period that includes the process of your electronic voting. For example, select the entry **"Last hour ’**.
5. Click on the menu button **"Delete now"**.

**Screenshot Step: 1**

![Screenshot Step: 1](../img/rules/firefox/de/1.png)
