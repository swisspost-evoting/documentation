# CHANGELOG
#
# Version May 2024
#
# Update to release 1.4
# Dynamically set the embedded Java Runtime Environment (JRE)
# Improve visibility of the extended authentication factor
# Add a check of the current directory before executing the script
# Improve handling of multiple directories and direct trust keystores
# Improve handling of empty user input
# Improve log messages
# Run the DIS script with the embedded JRE
#
# Version March 2024
# 
# Changelog
# Add mandatory directory.target property
#
# Version August 2023
# 
# Changelog
# Adding grace period setting
# Improving handling of Yes / No

Write-Host "Choose an Option by typing the desired character ([T] or [N]) and pressing ENTER."
$userprofile = $Env:userprofile
$arryes = @('ja', 'y', 'yes', 'j')
$arrno = @('nein', 'n', 'no')

#Set the parent directory
$parentDir = (Get-Item .).Parent.FullName
$parentDirName = (Get-Item .).Parent.Name

# Get the name of the current folder
$currentFolder = (Get-Item ".\").Name

# Check that the current folder is the data-integration-service folder
if (-not $currentFolder.StartsWith("data-integration-service")) {
    Write-Host "Error: This script must be executed within the data-integraton-service folder. Current folder: $currentFolder"
	Read-Host "Press Enter to exit"
    Exit
}

# Check that the parent folder is the PC-SETUP folder
if ($parentDirName -notin @("PC-SETUP")) {
    Write-Host "Error: This script must be executed in a subfolder of 'PC-SETUP'. Current parent folder: $parentDirName"
	Read-Host "Press Enter to exit"
    Exit
}


#Ask if the consistency date check should be overridden or not
do {
    $mode = Read-Host -Prompt "Start in [T]estmode or [N]ormal?"
} until ($mode -in "tn".ToCharArray())
switch ($mode) {
    "t" {
        Write-Host "Starting Testmode"
        $bypass = "true"
        do { $bypass2 = Read-Host "Do you want to define all counting circles as test? [Y] for Yes, [N] for No" } until ($arryes.Contains($bypass2.ToLower()) -OR $arrno.Contains($bypass2.ToLower()))
        if ($arryes.Contains($bypass2.ToLower())) { $bypass2 = "true" } else { $bypass2 = "false" }
    }
    "n" {
        Write-Host "Starting Normal mode"
        $bypass = "false"
		$bypass2 = "false"
    }
    Default {
        Write-Host "Error: Not a valid Mode"
        Pause
        exit 1
    }
}

do {
    $grace = Read-Host "Enter [Y] if you want to keep the default 900 seconds grace period. Otherwise, set the grace period by entering a number (seconds) between 0 and 3600"
    # Ensure input is either "Y" or a valid number
} until (($grace -match "^[0-9]+$" -and $grace -in 0..3600) -or ($arryes.Contains($grace.ToLower())))

if ($arryes.Contains($grace.ToLower())) { $graceperiod = 900 }
else { $graceperiod = $grace }

Write-Host "Grace Period set to $graceperiod"

#Get DIS jar in script root
$Daten = Get-ChildItem -Path $PSScriptRoot -Filter "data-integration-service-*.jar" | sort LastWriteTime -desc
$i = 1

#If multiple DIS are available, ask for which version shall be started
if ($Daten.Count -gt 1) {
	$array = @("")
	Write-Host "Multiple DIS-Files recognized"
	foreach ($d in $Daten) {
		$array += $d
		$datum = $d.LastWriteTime
		Write-Host "[$i] for $d - Change Date $datum" 
		$i++
	}
	
	$version = Read-Host "Which Version do you want to start?"
    $ver = $array[$version]
}

#If there is only one, define this one to be started
else {$ver = $Daten}

# Navigate to the parent directory of the current script
Set-Location $PSScriptRoot\..

# Get all directories starting with "_EE"
$eeFolders = Get-ChildItem -Directory -Filter "_EE*" | Sort-Object LastWriteTime -Descending

# Check if there are multiple "_EE" folders
if ($eeFolders -and $eeFolders.Count -gt 1) {
    Write-Host "Multiple '_EE' folders found. Please select one:"
    for ($i = 0; $i -lt $eeFolders.Count; $i++) {
        $folder = $eeFolders[$i]
        Write-Host "$($i + 1): $($folder.Name) - Last Modified: $($folder.LastWriteTime)"
    }

    # Prompt user to select a folder
    [int]$selection = 0
    while ($selection -lt 1 -or $selection -gt $eeFolders.Count) {
        $selection = Read-Host "Enter the number of the folder you want to select"
        if ($selection -notmatch '^\d+$' -or $selection -lt 1 -or $selection -gt $eeFolders.Count) {
            Write-Host "Invalid selection. Please select a number between 1 and $($eeFolders.Count)."
            $selection = 0
        }
    }

    # Set location to the selected folder
    Set-Location $eeFolders[$selection - 1].FullName
} elseif ($eeFolders.Count -eq 1) {
    # If there is only one "_EE" folder, select it automatically
    Set-Location $eeFolders[0].FullName
    Write-Host "The EE folder was automatically selected: $($eeFolders[0].Name) - Last Modified: $($eeFolders[0].LastWriteTime)"
} else {
    Write-Host "No '_EE' folders found."
}


#Remove old configuration files
Remove-Item DIS-output\*.* -Force

$DISInputFolder = Join-Path -Path (Get-Location) -ChildPath "DIS-Input"
$DISOutputFolder = Join-Path -Path (Get-Location) -ChildPath "DIS-Output"

#Check the input files
$ech0045 = (Get-ChildItem DIS-input -Filter "eCH0045v?*.xml").Count
$ech0157 = (Get-ChildItem DIS-input -Filter "eCH0157v?*.xml").Count
$ech0159 = (Get-ChildItem DIS-input -Filter "eCH0159v?*.xml").Count
$param = (Get-ChildItem DIS-input -Filter "*param*.xml").Count
$ech015x = $ech0157 + $ech0159
$echtot = (Get-ChildItem DIS-input -Filter "*eCH*.xml").Count

if ($echtot -gt ($ech0045 + $ech015x)) {
	Read-Host "There is a eCH file that is not named correctly or not recognized. Stopping the script. To exit please press Enter"
	exit 1
}

if ($ech015x -lt 1) {
	Read-Host "There are no election or vote eCH-files in the DIS-Input folder. Please check the DIS-Input folder. Stopping the script. To exit please press Enter"
	exit 1
}

if ($ech0045 -lt 1) {
	Read-Host "There is no electoral register file (eCH-0045.xml) in the DIS-Input folder. Please check the DIS-Input folder. Stopping the script. To exit please press Enter"
	exit 1
}

if ($param -ne 1) {
    Read-Host "There is no or multiple param.xml file in the DIS-Input folder. Please check the DIS-Input folder. Stopping the script. To exit please press Enter"
    exit 1
}

if ($param -eq 1) {
    $ea = (cat ((Get-ChildItem DIS-input -Filter "*param*.xml").FullName) | Select-String "AuthenticationKey")
    $hr = "-" * $Host.UI.RawUI.WindowSize.Width # Create a horizontal rule based on the console width
    if ($ea -like "*Year*") { 
        Write-Host $hr -ForegroundColor DarkGray
        Write-Host "Extended Authentication is set to`n" -NoNewline
        Write-Host "YEAR OF BIRTH" -ForegroundColor Red -NoNewline
        Write-Host "`n$hr" -ForegroundColor DarkGray
    } elseif ($ea -like "*Date*") { 
        Write-Host $hr -ForegroundColor DarkGray
        Write-Host "Extended Authentication is set to`n" -NoNewline
        Write-Host "DATE OF BIRTH" -ForegroundColor Red -NoNewline
        Write-Host "`n$hr" -ForegroundColor DarkGray
    } else {
        Read-Host "No Extended Authentication found. Fix the param.xml and restart the process. Press Enter to Exit"
        exit 1
    }
    do {
        $continue = Read-Host "Enter [Y] if the extended authentication factor is correct. Otherwise stop the process by entering [N] and correct the param.xml file"
    } until ($arrno.Contains($continue.ToLower()) -OR $arryes.Contains($continue.ToLower()))   
}

if ($arrno.Contains($continue.ToLower())) { exit 1 }

# Navigate to the parent directory of the current script
Set-Location $parentDir

# Get the current working directory
$currentDir = Get-Location

# Find all folders ending with 'direct-trust-canton'
$directories = Get-ChildItem -Path $currentDir -Directory | Where-Object { $_.Name -match 'direct-trust-canton$' }

# Check if any matching directories were found
if ($directories.Count -eq 0) {
    Write-Error "No folder ending with 'direct-trust-canton' exists."
    Read-Host "Press Enter to exit..."
    exit
}
elseif ($directories.Count -eq 1) {
    # Only one directory found, use it
    $directTrustCantonDir = $directories.FullName
}
else {
    # Multiple directories found, ask the user to choose
    Write-Host "Multiple directories found. Please select one by entering the corresponding number:"
    for ($i = 0; $i -lt $directories.Count; $i++) {
        Write-Host "$($i+1): $($directories[$i].FullName)"
    }

    $selection = Read-Host "Enter the number of the desired directory"
    # Validate selection
    while ($selection -lt 1 -or $selection -gt $directories.Count) {
        $selection = Read-Host "Enter the number of the desired directory"
    }

    # Set the selected directory
    $directTrustCantonDir = $directories[$selection - 1].FullName
}

# Continue script with the selected $directTrustCantonDir
Write-Host "Selected directory: $directTrustCantonDir"


# Get all .p12 and .txt files in the 'direct-trust-canton' directory
$p12Files = Get-ChildItem -Path $directTrustCantonDir -Filter "*.p12"
$txtFiles = Get-ChildItem -Path $directTrustCantonDir -Filter "*.txt"

# Check if there is exactly one .p12 file and exactly one .txt file
if ($p12Files.Count -ne 1 -or $txtFiles.Count -ne 1) {
    Write-Error "The 'direct-trust-canton' folder must contain exactly one .p12 file and one .txt file."
	Read-Host "Press Enter to exit..."
    exit
}

# Extract file paths
$keystorePath = $p12Files[0].FullName
$passwordPath = $txtFiles[0].FullName

# Print the paths
Write-Host "Keystore Path: $keystorePath"
Write-Host "Password Path: $passwordPath"

# Construct the argument list for the java command
$javaArguments = "-Dauthorization.gracePeriod=$graceperiod " +
                 "-Dbypass.checkConsistency.dates=$bypass " +
                 "-DcountingCircle.allTest=$bypass2 " +
                 "-Ddirect.trust.keystore.location.canton=`"$keystorePath`" " +
                 "-Ddirect.trust.keystore.password.location.canton=`"$passwordPath`" " +
                 "-Ddirectory.source=`"$DISInputFolder`" " +
                 "-Ddirectory.target=`"$DISOutputFolder`" -jar $ver"

Set-Location $PSScriptRoot

# Starting the DIS with the parameters
Start-Process "$PSScriptRoot\embedded-jre\bin\java.exe" -ArgumentList $javaArguments -NoNewWindow -Wait

If (Test-Path $DISOutputFolder\configuration-anonymized.xml) {
    Read-Host "Event created and configuration-anonymized.xml is present. Press Enter to close"
}
else { Read-Host "configuration-anonymized.xml file was not generated. Please check the logs" }