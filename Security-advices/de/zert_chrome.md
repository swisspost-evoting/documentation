# Google Chrome

## Überprüfung Zertifikat


1. Klicken Sie auf die Schaltfläche **"Website-Informationen anzeigen"** hier im Screenshot mit dem Pfeil markiert.
2. Klicken Sie nun auf dem offenen Menü auf **"Verbindung ist sicher"**.
3. Klicken Sie auf **"Zertifikat ist gültig"**.
4. Nun sehen Sie im neuen Fenster unter dem Reiter **"Allgemein"** die SHA-256-Fingerabdrücke. Für Sie wichtig ist der Fingerabdruck des Zertifikates. Bitte vergleichen Sie diesen Wert mit dem angegebenen Wert auf Ihrem Stimmrechtsausweis. Er muss identisch sein.

**Screenshot Schritt: 1**

![Screenshot Schritt: 1](../img/rules/chrome/de/z1.png)


**Hinweis:** Die Webadresse setzt sich stets wie folgt zusammen: ct.evoting.ch, wobei «ct» für das Kürzel des jeweiligen Kantons steht.
