# Apple Safari

## Stizzar la cronologia dal navigatur en Safari


1. Cliccai sin **"Safari"**.
2. Cliccai sin **"Verlauf löschen ..."**.
3. Elegiai en la glista da defilar almain il spazi da temp che cumpiglia il mument, cura che Vus avais votà electronicamain. Tscherni per exempel l'endataziun **"Letzte Stunde"**.
4. Cliccai sin **"Verlauf löschen"**.

**Screenshot Pass: 1 + 2**

![Screenshot Pass: 1+2](../img/rules/safari/de/1.png)

**Screenshot Pass: 3 + 4**

![Screenshot Pass: 3+4](../img/rules/safari/de/2.png)


