---

layout: article  
title: Technical specifications of the main components of the e-voting system  
author: © 2024 Swiss Post Ltd.  
date: "2024-10-22"  

toc-own-page: true  
titlepage: true  
titlepage-rule-color: "ffcc00"  
titlepage-text-color: "ffffff"  
footer-left: "Swiss Post"  
titlepage-background: ".gitlab/media/swisspostbackground.png"  

---

# Technical Specifications of the Main Components of the E-Voting System

This detailed list specifies all software and hardware components for the e-voting system.  
This list does not include security software components that are subject to constant updating and are part of the operating system used.  
Only components of the e-voting production environment are listed.  
Unless a version update fixes a critical vulnerability, we update OS versions in regular maintenance windows between election events.

**NOTE: Components that are subject to regular version updates are referred to as dynamic software and hardware.**

## Software and Hardware Components – Control Components Server

| Hardware                  | OS         | Version                       | Dynamic | 
|---------------------------|------------|-------------------------------|---------|
| ProLiant BL460c Gen10     | Redhat     | RHEL 8.10                     | No      |
| ProLiant BL460c Gen10     | Debian     | 11.11                         | No      |
| ProLiant BL460c Gen10     | Ubuntu     | 20.04                         | No      |
| ProLiant DL385 Gen10      | Windows    | Server 2022 Standard          | No      |

| Software                           | Version                      | Dynamic |
|------------------------------------|------------------------------|---------|
| Adoptium Temurin OpenJDK JRE       | 21.0.4                       | Yes     |

## Software and Hardware Components – Database Server

| Hardware                  | OS          | Version                      | Dynamic |
|---------------------------|-------------|------------------------------|---------|
| Cisco UCS C240 M6         | RedHat      | RHEL 8.10                    | No      |
| Cisco UCS C240 M6         | Windows     | Server 2022 Standard         | No      |
| ODA X8-2S                 | Proprietary | -                            | No      |

| Software                           | Version                      | Dynamic |
|------------------------------------|------------------------------|---------|
| Oracle Enterprise                  | 19.24                        | Yes     |

## Software and Hardware Components – Reverse Proxy Server

| Hardware                  | OS          | Version                      | Dynamic |
|---------------------------|-------------|------------------------------|---------|
| UCS B200 M6               | RedHat      | RHEL 8.10                    | No      |

| Software                           | Version                      | Dynamic |
|------------------------------------|------------------------------|---------|
| SE Linux                           | 2.9                          | Yes     |
| Apache                             | 2.4.62                       | Yes     |
| └ OpenSSL                          | 3.0.15                       | Yes     |
| └ mod_qos                          | 11.74                        | Yes     |
| ModSecurity                        | 2.9.8                        | Yes     |

## Software and Hardware Components – Kubernetes Cluster

| Hardware                  | OS          | Version                      | Dynamic |
|---------------------------|-------------|------------------------------|---------|
| Virtual Machine           | RedHat      | RHEL 8.10                    | No      |

| Software                           | Version                      | Dynamic |
|------------------------------------|------------------------------|---------|
| Kubernetes                         | 1.26                         | Yes     |
| Rancher                            | 2.7                          | Yes     |
| Docker                             | 26.1                         | Yes     |
| HAProxy-Ingress-Controller         | 1.10 (HAProxy 2.7)           | Yes     |

## Software Components of the E-Voting Software

| Software                           | Version                      | Dynamic |
|------------------------------------|------------------------------|---------|
| Adoptium Temurin OpenJDK JRE       | 21.0.4                       | Yes     |
| Apache HTTPD                       | 2.4.62                       | Yes     |
| Apache ActiveMQ Artemis            | 2.37.0                       | Yes     |

**Note: The libraries used (from Maven Central) to create the e-voting software components are part of the publication and are not listed separately here.**
 
