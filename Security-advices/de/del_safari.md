# Apple Safari

## Browserverlauf löschen in Safari

1. Klicken Sie auf den Menüpunkt **"Safari"**.
2. Klicken Sie auf den Menüpunkt **"Verlauf löschen ..."**.
3. Wählen Sie in der Dropdown-Liste mindestens den Zeitraum, der den Vorgang Ihrer elektronischen Stimmabgabe umfasst. Wählen Sie zum Beispiel den Eintrag **"Letzte Stunde"**.
4. Klicken Sie auf die Menütaste **"Verlauf löschen"**.

**Screenshot Schritt: 1 + 2**

![Screenshot Schritt: 1 + 2](../img/rules/safari/de/1.png)

**Screenshot Schritt: 3 + 4**

![Screenshot Schritt: 3 + 4](../img/rules/safari/de/2.png)