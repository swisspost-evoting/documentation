# Microsoft Edge

## En il modus privat activeschan en Microsoft Edge


1. Cliccai sin ils **"trais puncts"**, "prestaziuns e dapli" che marcà qua cun in pal en il monitur.
2. Cliccai sin **"Novas fanestras d'inprivata"**.
3. Vus lavurais en la **fanestra inprivata** che Vus enconuschais ils fundaments stgirs.

**Screenshot Pass: 1**

![Screenshot Pass: 1](../img/rules/edge/de/p1.png)
<br>

**Screenshot Pass: 3**

![Screenshot Pass: 3](../img/rules/edge/de/p2.png)