# Firefox

## Supprimer l’historique du navigateur dans Firefox



1. Cliquez sur les **"trois lignes horizontales"** en haut à droite, marquées d'une flèche dans la capture d'écran.
2. Choisissez l'option de menu **Historique**.
3. Cliquez sur l'option de menu **"Supprimer l'historique récent"**. (Combinaison de touches : Ctrl + Maj + Suppr)
4) Dans la liste déroulante, sélectionnez au moins la période qui couvre le processus de votre vote électronique. Sélectionnez par exemple l'entrée **"Dernière heure"**.
5. Cliquez sur le bouton de menu **"Effacer maintenant"**.

**Screenshot Étape: 1**

![Screenshot Étape: 1](../img/rules/firefox/de/1.png)