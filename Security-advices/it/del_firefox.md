# Firefox

## Cancellare la cronologia di navigazione in Firefox


1. Fare un clic sulle **"tre linee orizzontali”** in alto a destra, contrassegnate da una freccia nell'immagine.
2. Selezionare la voce di menu **Cronologia**.
3. Fare un clic sulla voce di menu **"Cancella la cronologia più recente”**. (combinazione di tasti: Ctrl + Maiusc + Canc).
4. Nell'elenco a discesa, selezionare almeno il periodo di tempo che comprende il processo del voto elettronico. Ad esempio, selezionare la voce **"Ultima ora”**.
5. Fare un clic sul pulsante di menu **"Elimina ora”**.

**Screenshot Passo: 1**

![Screenshot Passo: 1](../img/rules/firefox/de/1.png)
