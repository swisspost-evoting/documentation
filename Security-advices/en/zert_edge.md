# Instructions for checking the web address of the e-voting portal

Verification of the digital thumbprint in the browser

## Microsoft Edge


1. Click on the **lock symbol** next to the URL to display the website information, **marked with the arrow in the screenshot.**
2. Now click on **"Connection is secure"** in the open menu.
3. Click on **Certificate icon** at the top right of the menu.
4. You will now see the SHA-256 fingerprints in the new menu under the heading **"General"**. The fingerprint of the certificate is important for you. **Please compare this value with the value specified on your voting card (you can ignore colons).**

**Screenshot Step: 1**

![Screenshot Step: 1](../img/rules/edge/de/edge_zert1.PNG)


**Note:** The web address is always composed as follows: ct.evoting.ch, where ‘ct’ stands for the abbreviation of the respective canton.