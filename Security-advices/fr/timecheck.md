# Guide d'affichage et de synchronisation de l'heure système

## Général :
Pour permettre le vote, nous vous recommandons d'activer la synchronisation de l'heure sur votre appareil. Ci-dessous, vous trouverez des instructions succinctes pour différents systèmes d'exploitation.

## Windows :
- Cliquez sur l'heure en bas à droite > "Paramètres de date et d'heure."
- Réglez "Définir l'heure automatiquement" sur "Activé."
- Si aucun serveur de temps n'est configuré automatiquement, vous pouvez en ajouter un (par exemple, ntp.metas.ch).
- Si nécessaire, activez également l'option "Définir le fuseau horaire automatiquement."
- Cliquez ensuite sur "Synchroniser maintenant."

Votre heure devrait maintenant être synchronisée. Veuillez redémarrer votre navigateur maintenant.

## Mac :
- Cliquez sur le "Symbole Apple" en haut à gauche > "Préférences Système."
- Cliquez sur "Général" à gauche, puis sélectionnez "Date et heure."
- Activez "Définir automatiquement la date et l'heure," puis cliquez sur "Définir" et entrez un serveur de temps quelconque (par exemple, ntp.metas.ch).
- Si nécessaire, activez l'option "Définir automatiquement le fuseau horaire en fonction de votre emplacement."

Votre heure devrait maintenant être synchronisée. Veuillez redémarrer votre navigateur maintenant.