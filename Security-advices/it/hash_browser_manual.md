# Istruzioni per verificare nel browser i file HTML e JavaScript contenuti nel portale di voto elettronico

## Istruzioni per verificare nel browser i valori hash dei file HTML e JavaScript

- Qualora vogliate accertarvi che i file HTML e JavaScript utilizzati dal portale ufficiale per il voto elettronico siano invariati, esiste la possibilità di verificare ogni valore hash.
- Questo procedimento è da eseguire prima di esprimere il voto elettronico al fine di escludere la possibilità che i dati HTML e JavaScript vengano manipolati o falsificati (sostituiti).
- La seguente descrizione fa riferimento al file JavaScript **main.js** ma può essere utilizzata anche per altri file JavaScript. Per controllare il file HTML, è possibile visualizzare il codice sorgente della pagina cliccando col tasto destro del mouse nel browser e procedere direttamente al passaggio 5.
- Si prega di assicurarsi che, oltre ai file JavaScript elencati nel protocollo (**5 file JavaScript**) e al file index.html, nessun altro file JavaScript o HTML venga caricato.

### Procedura con calcolo manuale dei valori hash (SHA256-Hash)

1. Cercate l’**hash SHA256** dei file JavaScript **main.js** nei protocolli dei Cantoni e degli esperti che sono stati pubblicati. Il link ai protocolli si trova sul portale di voto sotto la voce **Trusted Build**.

2. Avviare il processo di votazione e aprire in seguito gli **Strumenti di sviluppo** del browser in uno dei seguenti modi:
   - Premete **F12** sulla tastiera o
   - Premete **Ctrl+Shift+I** sulla tastiera o
   - Cliccate sui **puntini di sospensione**, poi su **Altri strumenti** e in seguito su **Strumenti di sviluppo**

3. Cliccate sulla scheda **Sources** e selezionare il file **main.js**

4. Disattivare la funzione di stampa corretta (se l'icona è blu, allora è attiva).

![img](../img/rules/general/prettyprint.png)

5. Copiare il contenuto del file in una qualsiasi pagina di conversione dell'hash SHA256 e lasciare che calcoli l'hash **SHA256**.

6. Confrontate il **valore hash** visualizzato con il valore hash indicato nei protocolli pubblicati. Tenete presente che l’SHA-256 calcolato manualmente viene normalmente visualizzato in [formato Base16](https://www.rfc-editor.org/rfc/rfc4648#section-8), mentre il tag `Integrity` indica, conformemente alla [raccomandazione W3C](https://www.w3.org/TR/SRI/), il valore hash SHA-384 in [formato Base64](https://www.rfc-editor.org/rfc/rfc4648#section-4). Inoltre potete verificare anche i valori hash visualizzati sul portale di voto.

**Se le due sequenze di caratteri sono identiche, il file HTML o JavaScript non è stato modificato.**

