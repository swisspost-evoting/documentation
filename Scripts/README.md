# PowerShell scripts for executing command-line interface tools

The e-voting system includes a number of command-line interface tools: programs that enable users to interact with an application through a textual interface, using typed commands. 
These tools do not have a graphical user interface and require users to have a certain level of technical expertise. 
To enhance the usability of these tools, we offer PowerShell Scripts that simplify their execution. 
PowerShell is a command-line shell and scripting language developed by Microsoft, primarily used on Windows operating systems. 
PowerShell scripts are collections of commands and instructions written in the PowerShell language, used to automate and simplify administrative tasks. 
They can be executed from the command line or through a script file, and can interact with other system tools and applications. 
In the case of the e-voting system, PowerShell scripts are used to simplify the execution of the command-line interface tools and automate the copying of files to the designated directory for processing.

## [Script for the Data Integration Service (DIS)](DIS)

This PowerShell script executes the Data Integration Service. 
Moreover, the script provides a test mode where the data-integration-service does not check that the election event data is in the future.
The Data Integration Service's description as well as its source code can be found [here](https://gitlab.com/swisspost-evoting/e-voting/data-integration-service).
