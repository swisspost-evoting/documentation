# Symbolic Analysis of the Swiss Post Voting System

We provide a symbolic model of the Swiss Post Voting System and use the cryptographic protocol verifier ProVerif to prove verifiability (both individual and universal) and vote privacy.

## Introduction

Modern cryptography uses two fundamental tools to reason about the security of cryptographic protocols: Cryptographic models and symbolic models [6]. In the cryptographic model, messages are bitstrings from a uniform distribution, cryptographic primitives are algorithms satisfying some asymptotic properties, and one reasons about the adversary's advantage. On the other hand, in the symbolic model, messages are terms from some algebra, cryptographic primitives are rewrite rules or equations, and one models the adversary as a state machine [2]. Symbolic models can quickly find errors in cryptographic protocols or demonstrate their correctness since they enable a high degree of automation in tools [3].

In addition to the symbolic models and proofs described here, we also provide a [cryptographic proof](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/tree/master/Protocol) of the Swiss Post Voting System.

We model the Swiss Post Voting System using ProVerif: a state-of-the-art tool for automated reasoning on symbolic models that has been used to model numerous cryptographic protocols [4].  ProVerif allows modeling cryptographic operators and proving security properties such as authentication, equivalence, and reachability.  You can find a list of publications as well as tutorials and an installation guide on the [ProVerif website](https://prosecco.gforge.inria.fr/personal/bblanche/proverif/).

## Compatibility

The symbolic models align to the [system specification](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/tree/master/System) version 1.4.

## Security Objectives

We derive the following security objectives from the Federal Chancellery's ordinance on electronic voting (VEleS - Verordnung über die elektronische Stimmabgabe) and its technical annex (VEleS annex):

* Individual Verifiability
* Universal Verifiability
* Privacy

### Individual and Universal Verifiability

The VEleS annex states the following security objective:

> 2.5 The voter is given proofs [...] to confirm that no attacker:
>
> * has altered any partial vote before the vote has been registered as cast in conformity with the system;
> * has maliciously cast a vote on the voter's behalf which has subsequently been registered as a vote cast in conformity with the system and counted.
>
> 2.6 The auditors receive a proof [...] to confirm that no attacker:
>
> * after the votes were registered as cast in conformity with the system, has altered or misappropriated any partial votes before the result was determined
> * has inserted any votes or partial votes not cast in conformity with the system which were taken into account in determining the result.

In the symbolic setting, we model these security objectives by properties on the occurrences of the events HappyUser and HappyAuditor. The system never reaches a state where the untrustworthy components (voting client, voting server, and all but one control component) successfully inserted, altered, or dropped a vote **without** detection by a voter or an auditor.

The individual verifiability models execute within the following scenario:

* 2 CCRs (Return code control components), 2CCMs (Mixing control components)
* Setup Component is trustworthy
* the voter Alice and CCR1 are trustworthy
* CCR2, the voting server, the voting client, CCM1 and CCM2 are untrustworthy

We provide two ProVerif Files for proving individual verifiability:

* [Individual-Verifiability-2CCRs-n=4-psi=1.pv](./verifiability/Individual-Verifiability-2CCRs-n=4-psi=1.pv): 4 voting options and voters selecting 1 voting option.
* [Individual-Verifiability-2CCRs-n=4-psi=2.pv](./verifiability/Individual-Verifiability-2CCRs-n=4-psi=2.pv): 4 voting options and voters selecting 2 voting options.

Moreover, we provide a ProVerif file [Individual-Verifiability-2CCRs-n=4-psi=1-Haines-attack.pv](./verifiability/Individual-Verifiability-2CCRs-n=4-psi=1-Haines-attack.pv) that models the attack against individual verifiability that Thomas Haines found in an earlier version of the cryptographic protocol (see [Gitlab issue #2](https://gitlab.com/swisspost-evoting/documentation/-/issues/2)). ProVerif [finds the attack](./verifiability/results_verifiability/results_individual_verifiability_attack_haines.txt) and provides an additional validation of the symbolic model of individual verifiability. Please note that the attack is modelled based on version 0.9 of the symbolic proofs.

The file [Universal-Verifiability-2CCMs-n=4-psi=2.pv](./verifiability/Universal-Verifiability-2CCMs-n=4-psi=2.pv) captures the model for universal verifiability and runs in the following scenario:

* 2 CCRs (Return code control components), 2CCMs (Mixing control components)
* 4 voting options and voters selecting 2 voting options
* CCR1, CCM2 and voter Alice are trustworthy
* CCR2, CCM1, the voting server and the voting client are untrustworthy

### Vote privacy

The security objective in the VEleS annex regarding vote privacy is the following:

> 2.7 It must be ensured that no attacker is able to breach voting secrecy or establish premature results unless he can control the voters or their user devices.

Since a malicious voting client can spy on voter's choices, we limit our threat model for vote privacy to a trustworthy voting client. Furthermore, we exclude trivial privacy breaches, such as when all honest voters select the same voting options or when the ballot box contains only one vote.

If the operational procedures ensure that the adversary cannot run the tally phase prematurely, voting secrecy implies excluding early provisional results.

In line with standard practice in symbolic analysis, we model vote privacy as the observational equivalence of two processes: the attacker does not see the difference when Alice votes j0 and Bob votes j1, from the case when the votes are swapped (Alice votes j1, Bob votes j0) [5].

The privacy model runs with n (unbounded) voting options out of which the voter selects one and under the following scenario:

* two honest voters (Alice and Bob) plus one dishonest voter. Arapinis et al. showed, in some contexts, that proving privacy for three voters is sufficient [1].
* the Setup Component is trustworthy
* the voting clients of Alice and Bob are honest
* each honest CCM checks the cleansing as well as all proofs of correct mixing from the previous CCMs
* all CCR are untrustworthy as soon as the voting phase starts (all material is honestly generated during the setup)
* the voting server is dishonest

The ProVerif model for privacy contains two files:

* [vote_privacy_CCM1.pv](./privacy/vote_privacy_CCM1.pv): CCM1 is trustworthy, the other CCMs are untrustworthy
* [vote_privacy_CCM2-3-4.pv](./privacy/vote_privacy_CCM2-3-4.pv): at least one CCM (CCM2, CCM3, or CCM4) is trustworthy

## Abstractions / Limitations

We describe some of our modeling choices to delimit our symbolic analysis better. Moreover, we would like to highlight that the symbolic models are subject to the same limitations with regard to quantum computing, a trustworthy voting client for privacy, and a trustworthy setup component, see the [section limitations of the computational proof](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/tree/master/Protocol#Limitations).

### Encryption of partial Choice Return Codes

In the Swiss Post Voting System, the voting client encrypts the partial choice return codes with the Choice Return Codes Encryption public key, and the control components jointly decrypt them. Our symbolic model omits the encryption of the partial Choice Return Codes and strengthens the adversary since the voting client sends the partial choice return codes in plaintext; we simplify the model without impacting the analyzed security properties.

### Tally control component

The symbolic model omits the Tally control component since we consider the Tally control component an untrustworthy element. In the actual implementation, the Tally control component acts as a "weak" control component that verifies the voting client's zero-knowledge proofs and the online control components' shuffle and decryption proofs.

### Auditors / Verifier

We omit to model the role of the auditors and their technical aid (verifier). 
The auditors perform multiple verifications after the configuration and the tally phase. 
However, the auditors only verify information; they do not process data that serves as input for other algorithms.
In the symbolic setting, the auditors ensure that specific properties are always true; for instance, the control components correctly mixed and decrypted the votes. 
Hence, we model the auditors' checks as conditions in the ProVerif queries and avoid bloating the models with an additional actor with a limited role.

### Electoral Board Key Pair and Hashes

As an operational safeguard, the Swiss Post Voting System derives the Electoral Board secret key 
from the passwords of the electoral board members. 
For usability reasons, the Tally control component checks the correctness of the passwords using hashes 
based on the Argon2id key derivation function.
From the trust model's point of view, 
however, the electoral board together with the tally control component is either trustworthy or not.
Therefore, we omit the distributed nature of the electoral board secret key and the corresponding hashes.

### Public parameters

Similar to the computational model, we assume that each party knows the election's public parameters, such as the list of voters, the number of voting options, and the applicable rules for selecting voting options.
In practice, the auditors enforce the consistency of this information: They check the generation of voting cards, the verifiability of public cryptographic parameters, the selection of prime numbers to represent voting options, etc.

### Write-in votes

In Switzerland, most election events have a fixed set of voting options; even if write-in options are allowed, only a tiny percentage of voters use write-ins.

The Swiss Post Voting System supports write-in candidates: using multi-recipient ElGamal encryption, the system encodes
and encrypts write-in candidates as separate messages along with the pre-defined candidates.
However, the protocol cannot provide individual verifiability for the write-in candidates 
since it is impossible to map all possible write-in values to a Choice Return Code.

Since the voting client encodes write-ins as separate ciphertext elements, it is evident that the presence of write-in votes does not impact the verifiability of the chosen pre-defined candidates.
Moreover, the presence of write-ins does not impact vote privacy either because the voting client's zero-knowledge proofs include the contents of the write-ins.
Hence a malicious voting server cannot undetectably change the write-in ciphertexts without controlling the voting client.
Thus, the zero-knowledge proofs prevent an adversary from marking a vote in order to break vote privacy.
Bear in mind that for vote privacy, the voting client is considered trustworthy: an attacker controlling the voting client could break vote privacy anyway by spying on the voter's choices.

For all these reasons, we limit our symbolic analysis to selecting pre-defined voting options; we exclude write-in options from our model.

### Effective Authentication and Voter Authentication

The Federal Chancellery's Ordinance defines the property of **effective authentication**

> 2.8 It must be ensured that no attacker can cast a vote in conformity with the system without having control over the voters concerned.

By the nature of our two-round return code scheme, the security objective of **effective authentication** follows naturally from the property of individual verifiability
since an attacker cannot confirm a vote without knowing the voter's ballot casting key (BCK).

### Channel Security

The Swiss Post voting system requires each protocol participant to cryptographically sign its messages and to verify the other participants' messages 
upon reception. As a consequence, the Swiss Post Voting System requires an out-of-band channel to exchange certificates and to ascertain the authenticity 
of each other's certificate (direct trust approach). The system specification as well as the architecture document of the Swiss Post Voting System detail 
how the Swiss Post Voting System achieves channel security.

The Federal Chancellery's ordinance explains that one can assume trustworthy channels to exchange certificates.

> 2.13.3 It may be assumed that trustworthy channels exist to distribute electronic certificates among system participants.

Therefore, the symbolic model omits modelling the out-of-band channel and the corresponding exchange of certificates.

## Results

We demonstrate that the Swiss Post Voting System guarantees vote privacy and individual and universal verifiability within our symbolic model.
While we encourage the reader to run the symbolic models in ProVerif by themselves, we provide the ProVerif (Using ProVerif 2.05) output here:

* [Verifiability](./verifiability/results_verifiability)
* [Privacy](./privacy/results_privacy)

## Changes since 2019

Since the initial publication of 2019, we improved the following aspects of the symbolic models:

* We resolved some executability issues in the original models.
* We relaxed some of the trust assumptions in the symbolic model.
* We made the symbolic models more robust by performing additional sanity checks.
* We improved the model's readability by adding clearer comments and explanation.
* We made the symbolic models compatible with the latest version of ProVerif.
* We aligned the models to the new Ordinance and the latest version of the system specification.

## Change Log

An overview of all major changes within the published releases is available [here.](CHANGELOG.md)

## Future Work

The symbolic proofs implement the Swiss Post Voting System faithfully and according to best practices in symbolic modelling. Nevertheless, we plan the following improvements in future versions of the ProVerif models.

* Unify the verifiabilty and privacy models. For instance, the symbols tild() and pCC() both model an exponentiation and should be merged, i.e. tild(k,x) = pCC(k,x) = x^k.
* (Individual Verifiability) In addition to the individual verifiability property, one could consider a "recorded-as-intended" property, i.e. a property of the form CCRconfirmed() ==> VoterSentConfirm() || dishonestVoter()
* (Universal Verifiability) Model the auditor's verification algorithms more faithfully. Currently, the symbolic models simplify these aspects by modelling the result of the verification algorithms instead of modelling the verification algorithms themselves.
* (Privacy) Model the VerifyConfigPhase algorithm more faithfully. Currently, our model assumes that the CCR behave honestly during the configuration phase. The fact that the auditors verify the CCR's operations during the configuration phase justifies this modeling decision, but a more faithful representation would be desirable.
* Make the models more general by increasing the number of possible voting options and by modelling multiple ballot boxes.
* Prove additional properties (beyond those required by the Ordinance). 
* (Verifiability) The protocol uses encryption keys built by the merge of several keys using the symbol 'mergepk(k1,k2)'. The model currently defines a full decryption rewriting rule DecMerge(sk1, sk2, (Enc_c1(R), Enc_c2(mergepk(pube(sk1), pube(sk2)), M, R))) = M but does not allow a step-by-step decryption. It might be improved considering the two rules:
    * Dec(sk1, Enc(mergepk(pube(sk1),pube(sk2)), M, R)) = Enc(pube(sk2), M, R); and
    * Dec(sk2, Enc(mergepk(pube(sk1),pube(sk2)), M, R)) = Enc(pube(sk1), M, R).

## Authorship and License

(c) Copyright 2024 Swiss Post Ltd.

Based on original work by Scytl Secure Electronic Voting S.A. (succeeded by Scytl Election Technologies S.L.U.), modified by Swiss Post. Scytl Election Technologies S.L.U. is not responsible for the information contained in this document.

Swiss Post improved the symbolic models in collaboration with Véronique Cortier, Alexandre Debant, and Pierrick Gaudry from CNRS/LORIA. We want to thank the researchers from CNRS/LORIA for their expertise and kind support with the ProVerif models.

We would also like to thank David Galindo (Crypto in Motion Ltd) and Eike Ritter for their contribution to the initial verifiability model and Carsten Schürmann for his valuable comments on an earlier version of the symbolic proofs.

E-Voting Community Program Material - please follow our [Code of Conduct](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/blob/master/CONTRIBUTING.md) describing what you can expect from us, the Coordinated Vulnerability Disclosure Policy, and the contributing guidelines.

## Revision History

| Version | Description                                   |  Author | Reviewer | Date       
|---------|-----------------------------------------------| ------------- |----------|------------|
| 0.8     | Original Work by Scytl R&S for sVote 2.2.1    | EL, TF, NC | JP       | 2020-04-03 |
| 0.9     | Initial published version                     | OE | CK, XM   | 2021-08-23 |
| 1.0     | Update to new protocol version                | OE | XM       | 2022-07-05 |
| 1.1     | Minor update to incorporate experts' feedback | OE | AH       | 2022-12-09 |
| 1.2     | Modeled voter authentication                  | OE | AH       | 2023-04-14 |
| 1.2.1   | Small fix in the voter authentication         | OE | AH       | 2023-06-08 |
| 1.3     | Aligned models to system specification 1.4    | OE | AH       | 2024-02-14 |

## References

[1] M. Arapinis, V. Cortier, and S. Kremer: "When are three voters enough for privacy
properties?" In: European Symposium on Research in Computer Security. Springer. 2016,
pp. 241–260.

[2] D. Basin and S. Capkun: Review of Electronic Voting Protocol Models and Proofs -
Combined Final Report. 2017.

[3] D. Basin, J. Dreier, and R. Sasse: Automated symbolic proofs of observational equivalence.
2015.

[4] B. Blanchet: Modeling and verifying security protocols with the applied pi calculus and
ProVerif. 2016.

[5] V. Cortier, D. Galindo, and M. Turuani: “A formal analysis of the Neuchâtel e-voting
protocol". In: 2018 IEEE European Symposium on Security and Privacy (EuroS&P).
IEEE. 2018, pp. 430–442.

[6] J. Herzog: "A computational interpretation of Dolev–Yao adversaries". In: Theoretical
Computer Science 340.1 (2005), pp. 57–81.
