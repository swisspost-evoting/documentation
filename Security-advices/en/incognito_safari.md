# Apple Safari

## Private surfing mode in Safari


1. Click on the top on **File** to open the **Options**.
2. Click on **New Private Window**.
3. Continue working in the new **private window**, which you recognise by the **private** marking.

**Screenshot Step: 1 + 2**

![Screenshot Step: 1 + 2](../img/rules/safari/en/p1.png)
<br>

**Screenshot Step: 3**

![Screenshot Step: 3](../img/rules/safari/en/p2.png)