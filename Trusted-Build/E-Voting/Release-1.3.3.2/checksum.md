
| Artefact     | Version    | Checksum    |
| --------|---------|---------|
| **config-cryptographic-parameters-tool-1.3.3.2.jar**  |  1.3.3.2  |  06cf0df9c1083525d813b295c192cc5229b5ae1e2b99796f758c3d922739f90f |
| **xml-signature-1.3.3.2.jar**  |  1.3.3.2  |  4f03216260f08df63cb31f5126e2fb43c6b50c17d6a95e12bb25a89330ea229f |
| **secure-data-manager-package-1.3.3.2.zip**  |  1.3.3.2  |  dec7d686f611db8fd83a1ab4be34d436822c098b776e5861f3d92e91e1e127b6 |
| **voter-portal-1.3.3.2.zip**  |  1.3.3.2  |  8dc9adbf45374a20708554d93334516f8e67d07bc3f0d7f61d4e6a835823bafa |
|    --> index.html  |  1.3.3.2  |  63577764dd6d348e6db9ed29500c82de369440ba0c8feccf6cb61c6256e4fb30 |
|    --> crypto.ov-api.js  |  1.3.3.2  |  2faf9fae8d6bf75cc1ca06aa9e4f5f2ae4f220420e9d047d884b3cac7bdb26a3 |
|    --> crypto.ov-api.js  |  1.3.3.2  |  sha384-7G9+mPIpCYXzspoKVxrbB9gypA8LQN3AcdpKeGkl0JF/fMDXMpwPEuJuy43BX6Eb |
|    --> crypto.ov-worker.js  |  1.3.3.2  |  f402c2ee8136d11d014618ed94a6654dd396195dbfca1c775d149b3b23621597 |
|    --> crypto.ov-worker.js  |  1.3.3.2  |  sha384-riFOXw/7W7NhBIOrLmR5nYV3ERljUX1vp/w5YS0dPWCjnRCP4GsdvIQqxFhwsDRR |
|    --> main.js  |  1.3.3.2  |  d7564715dac28ecf972c1bc81e5e8980c7e94945b935445433c003def4e5d0f6 |
|    --> main.js  |  1.3.3.2  |  sha384-4s2RRwKzdg4h375qYVuBDzlwTwV0CKWEEEbEZ3LUmzaZ6qGLz+3XiLumaK4bSPGk |
|    --> polyfills.js  |  1.3.3.2  |  34dfb6e7c7123409169c1b505914dcf3a93b020cddf6d920af248aba9325b99a |
|    --> polyfills.js  |  1.3.3.2  |  sha384-ADkau9TRfRJgjPSietYkJtX6HXQO0rHfv3Grxl4mrWOXJStCoIp3NYVehF+jV2tb |
|    --> runtime.js  |  1.3.3.2  |  12aabd163c442aae3e55119869e24e35654310fb3de1ced5aaf68f6400956ef1 |
|    --> runtime.js  |  1.3.3.2  |  sha384-dl5N3uRQoV0Gvcn8R0Iwi5XWok4Hru2MaccA0GUVXpz+8KUybTBAJ88CJCSpQGiR |
| **voting-server-1.3.3.2-runnable.jar**  |  1.3.3.2  |  8ffdb75081d1c9744db12d8d39d519a81978a465ad08e944326da98d4f6e9ccd |
| **control-component-runnable.jar**  |  1.3.3.2  |  fbd505224accd7ebe2a9e4028df1990e3b739f7c78a018fcbd879b16fc4e777a |