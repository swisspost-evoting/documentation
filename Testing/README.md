# What is the content of this folder?

| Document | Content | Download |
| :------- |:--------|:--------:|
| [Test Concept of the Swiss Post Voting System](Test%20Concept%20of%20the%20Swiss%20Post%20Voting%20System.md) | This document describes the test concept for the e-voting service. The test concept shows the test procedure, defines the cooperation and the responsibilities of all persons involved in the test process. | <a href="https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/jobs/artifacts/master/raw/Test%20Concept%20of%20the%20Swiss%20Post%20Voting%20System.pdf?job=CONVERT"><img src="../.gitlab/media/icons/button_download-pdf.png" alt="Download document as PDF" width="186" height="30"></a> |