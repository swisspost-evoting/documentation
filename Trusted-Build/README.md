## Trusted Build of the Swiss Post Voting System

This folder contains all the relevant information needed to perform your own trusted build and compare your hash results with the ones from Swiss Post.

| Folder | Content | Type of file |
|:-------|:--------|:-------------|
|[DIS](DIS) | Includes hash values, scripts and log files of the trusted build run of each release of the Data Integration Service. | .md, .html, .txt, .pdf |
|[E-Voting](E-Voting) | Includes hash values, scripts and log files of the trusted build run of each release of E-Voting. All protocols for the Trusted Build and Trusted Deployement are published in the corresponding releases within this folder. | .md, .html, .txt, .pdf |
|[Verifier](Verifier) | Includes hash values, scripts and log files of the trusted build run of each release of Verifier. | .md, .html, .txt, .pdf |

| Document | Content | Download |
|:---------|:--------|:---------|
|[Trusted Build of the Swiss Post Voting System](Trusted%20Build%20of%20the%20Swiss%20Post%20Voting%20System.md) | This document explains the trusted build implementation, shows the process and contains information about the risk assessment regarding the software build. | <a href="https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/jobs/artifacts/master/raw/Trusted%20Build%20of%20the%20Swiss%20Post%20Voting%20System.pdf?job=CONVERT"><img src="../.gitlab/media/icons/button_download-pdf.png" alt="Download document as PDF" width="186" height="30"></a> |

The correspondence between the Verifier and E-voting system version is indicated [here](https://gitlab.com/swisspost-evoting/verifier/verifier#e-voting-compatibility).
