# Roadmap Swiss Post's e-voting system

The roadmap sets out the dates for the availability of releases and documents that are planned to be made available. 

In addition, the information on GitLab is always up to date:

- [Overall Changelog](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/blob/master/CHANGELOG.md)
- [Readme E-voting](https://gitlab.com/swisspost-evoting/e-voting/e-voting/-/blob/master/README.md)
- [Readme E-Voting Libraries](https://gitlab.com/swisspost-evoting/e-voting/data-integration-service/-/blob/develop/README.md)
- [Readme Crypto-Primitives](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives/-/blob/master/README.md)
- [Readme Crypto-Primitives Typescript](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives-ts/-/blob/master/README.md)
- [Readme Verifier](https://gitlab.com/swisspost-evoting/verifier/verifier/-/blob/master/README.md)
- [Readme Data Integration Service](https://gitlab.com/swisspost-evoting/e-voting/e-voting-libraries/-/blob/master/README.md)


----------

## Upcoming Releases
The main features of the upcoming releases are listed below:

### E-voting System, 2025, Release 1.5
- Improvements from the independent examination [mandated by the Federal Chancellery](https://www.bk.admin.ch/bk/en/home/dokumentation/medienmitteilungen.msg-id-84337.html)*

### E-voting System, 2026, Release 1.6
- Improvements from the independent examination [mandated by the Federal Chancellery](https://www.bk.admin.ch/bk/en/home/dokumentation/medienmitteilungen.msg-id-84337.html)*


(*) the measures of the "[*Catalogue of measures by the Confederation and cantons*](https://www.bk.admin.ch/dam/bk/de/dokumente/pore/Vote--lectronique/Vote%20%C3%A9lectronique%20Massnahmenkatalog%20von%20Bund%20und%20Kantonen%20vom%2020.02.2023.pdf.download.pdf/Vote%20%C3%A9lectronique%20Massnahmenkatalog%20von%20Bund%20und%20Kantonen%20vom%2020.02.2023.pdf)" from the Federal Chancellery are currently not listed in detail.