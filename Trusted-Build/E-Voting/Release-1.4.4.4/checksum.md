
| Artefact     | Version    | Checksum    |
| --------|---------|---------|
| **direct-trust-tool-1.4.4.4.zip**  |  1.4.4.4  |  b7c11631789dbb217d420b73c895f97834e157596d25015a38e0f453997d4f4d |
| **xml-signature-1.4.4.4.jar**  |  1.4.4.4  |  7983ed0f0e75536466cc6adcd5ffde630cffd6ab90016d1c810ba70fac81427d |
| **file-cryptor-1.4.4.4-runnable.jar**  |  1.4.4.4  |  cbfd46e1bcfb9f5a892bb6bc5cc28fb8dfabd9c9f57034f0d5391c372dcb2e66 |
| **control-component-runnable.jar**  |  1.4.4.4  |  910db2b50f058742a4a173b6e88efe2a773ec484e0f0d8a91dfcd421269061ec |
| **voter-portal-1.4.4.4.zip**  |  1.4.4.4  |  923dfc84a9633fa6376bd8d1a8ea5c891311b001b9314f3b924e0ec42cd9afeb |
|    --> index.html  |  1.4.4.4  |  d96b7cdb46d3de04cca1257d151466df9d511cf78ad2abe250ff645acdc994cb |
|    --> crypto.ov-api.js  |  1.4.4.4  |  bd32848a0e07e2dccef465d47d4e295ed079be1dbee43f652bec1294fadede48 |
|    --> crypto.ov-api.js  |  1.4.4.4  |  sha384-9alQr57CZH3awteoDnjYkHFhdgE7OQlCh+RWiTSx39sM989KfgIvmYiaB/QEmQ8J |
|    --> crypto.ov-worker.js  |  1.4.4.4  |  808104714101ddec957022f16c00f4aea7a5967f45da9d5512475db4c40fa5cc |
|    --> crypto.ov-worker.js  |  1.4.4.4  |  sha384-AqfFbQGoWuNqQKdN5/yWUiSeBTcgTRKoFZjvGv6y54l2pb+goOGxKnqVrIIsCH11 |
|    --> main.js  |  1.4.4.4  |  6f12c219ab543a03c73093a9b0c56ba14abe0cd83b7a5526b1b5c3e273f5c660 |
|    --> main.js  |  1.4.4.4  |  sha384-XytkG89QKqR/DN9HyXd0w0GX5JT+hMvuxQoqZIi4EjRyMg/eaXG8E4+z1QK6hHUY |
|    --> polyfills.js  |  1.4.4.4  |  4c6def3377663b9a437cb43a981b4c36edf5cbb78b956fd7ebbe1754d362860f |
|    --> polyfills.js  |  1.4.4.4  |  sha384-ZIVSoFeZ4Rl8BJTsjulnwRKxIPbb9acPMCKPB1hC9gPCGaFd7jusR1B90wXapEKz |
|    --> runtime.js  |  1.4.4.4  |  ea965b429bb063139b86122fa981ec206efd2aa0834f13154580522967d0ecc5 |
|    --> runtime.js  |  1.4.4.4  |  sha384-YwZU+M0RWirxGUXpR82bV38PfKIXCa1y7oI8xk3Xo3I+rHG5znVHx9+02CX0YUS6 |
| **secure-data-manager-package-1.4.4.4.zip**  |  1.4.4.4  |  bb1ff024102c5564aef90386c0c01dc0ea67c8c19f193853aea5f3f923338d0e |
| **voting-server-1.4.4.4-runnable.jar**  |  1.4.4.4  |  c0a6300d343cedb38f10a418388b985dcf94627534292f46f7c74e72d4c03806 |