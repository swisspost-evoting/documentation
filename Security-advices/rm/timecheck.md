# Gida per mussar e sincronisar l'ura dal system

## En general:
Per pussibilitar il vot, nus recommendain d'activar la sincronisaziun da l'ura en tia apparecchiatura. Suttamessa èn instrucziuns curtas per differentas sistems d'exploitaziun.

## Windows:
- Fai in clavuotscha sin l'ura sut a fin a dretg > "Configuraziun da data ed ura."
- Metti "Activar l'ura automaticamain" sin "Activà."
- Sche nagut server da temp è chattà automaticamain, possas ti metter in zifer dals glistess (per exempel, ntp.metas.ch).
- Sche necessari, activa er l'opziun "Activar l'uzanca da l'ura automaticamain."
- Lura fa in clavuotscha sin "Sincronisar ussa."

Suenter duess l'ura esser sincronisada. Cumenzai ussa da nov il navigatur.

## Mac:
- Fai in clavuotscha sin il "Simbul da Mac" sut a fin a sanester > "Preferenzas da System."
- Fai in clavuotscha sin "General" a sanester e selecziuna suenter "Data e ura."
- Activescha "Activar data ed ura automaticamain," lura fa in clavuotscha sin "Metter" ed inscrescher in server da temp qualsiasi (per exempel, ntp.metas.ch).
- Sche necessari, activescha l'opziun "Activar la zona da temp automaticamain sin basa da la to posiziun."

Suenter duess l'ura esser sincronisada. Cumenzai ussa da nov il navigatur.