# Security advices

Voters can ensure the e-voting process is successful by voting according to the instructions. In particular, this includes comparing the choice return codes generated before casting the vote with the codes on the physical voting documents. To cast the vote, the ballot casting key from the physical voting documents is typed onto the device being used to vote. The vote cast code, which is displayed after the vote has been cast, must also be compared with the vote cast code on the physical voting documents. This process is known as individual verifiability. This allows voters to check their vote has been cast unaltered, and that it has been registered correctly in the electronic ballot box.

Additionally, voters can carry out various checks on the device used for voting to ensure their vote has not been manipulated.

The various measures are laid out below.

## Checking the certificate’s fingerprint

If you want to check that you are on the correct official E-voting portal, you can check the certificate’s fingerprint. That way you can make sure that you are not on a manipulated or fake website. Certificates are used to guarantee the authenticity of the web server that you are using and to encrypt the communication connection with the server. This enables you to distinguish between genuine and fake websites, even if they may superficially look similar or the same. **Please be aware that IOS device do not support it for the moment.** 

| [![google-chrome](../icons/google-chrome.png)](zert_chrome.md) | [![Microsoft-Edge](../icons/microsoft-edge.png)](zert_edge.md) | [![firefox](../icons/firefox.png)](zert_firefox.md) | [![apple-safari](../icons/apple-safari.png)](zert_safari.md) |
| ----------------------------------------- | ------------------------------------------------------------ | ---------------------------------------- |-------------------------------------------------------------|
| [Google Chrome](zert_chrome.md)           | [Edge](zert_edge.md)                                         | [Firefox](zert_firefox.md)               | [Safari](zert_safari.md)                                     |       |


### What do I do if my browser displays an incorrect fingerprint?

You should stop the voting process and inform the canton (support team).

An incorrectly displayed web page certificate fingerprint means that there is no direct connection between the web browser and the voting platform. In most cases, the interruption is actually the result of a protective measure rather than being linked to an attempt at fraud. For example, certain companies disrupt the connection between employees' computers and the Internet platforms in their networks to filter data traffic for harmful data. Virus protection programs can also be used in this way and configured accordingly.

## Deleting browsing history

If you want to make sure that no conclusions can be drawn about your vote on your device (computer, tablet, smartphone), we recommend clearing the browser cache after every vote. For instructions on how to delete browsing history, click on the browser that you use below.

| [![google-chrome](../icons/google-chrome.png)](del_chrome.md) | [![Microsoft-Edge](../icons/microsoft-edge.png)](del_edge.md) | [![firefox](../icons/firefox.png)](del_firefox.md) | [![apple-safari](../icons/apple-safari.png)](del_safari.md) |
| ----------------------------------------- | ------------------------------------------------------------ | ---------------------------------------- |-------------------------------------------------------------|
| [Google Chrome](del_chrome.md)           | [Edge](del_edge.md)                                         | [Firefox](del_firefox.md)               | [Safari](del_safari.md)                                     |       |
| [Chrome Mobile](del_chromemobile.md)              |           [Edge Mobile](del_edgemobile.md)                                                           |                    |   [Safari Mobile](del_safarimobile.md)                                     |            |

## Verify hash values (extended check)

To ensure the HTML and JavaScript code on the voter’s device carries out the intended operations and encodes the vote correctly, the voter can verify the integrity of the HTML and JavaScript code used. To do this, voters can compare the hash values of the JavaScript files in question with the values published in the protocols of the cantons and the external experts as well as on the e-voting portal.
Please note that the integrity of the HTML code can only be manually verified.

| [![Microsoft-Edge](../icons/microsoft-edge.png)](hash_browser_automatic.md) |
| ------------------------------------------------------------ |
| [automatic verification (SHA384-Hash)](hash_browser_automatic.md)
| [manual verification (SHA256-Hash)](hash_browser_manual.md)   

## Use browser mode without add-ons (Incognito or Private Mode)

As an additional precaution, we recommend that you use a browser mode which prevents browser add-ons by default. In Chrome and Microsoft Edge, this is known as incognito mode, in Internet Explorer as InPrivate Browsing and in Safari as Private Browsing. You can also check and deactivate add-ons and browser extensions yourself if necessary.

**Please disable the automatic translation function of your browser to ensure that the contents of the e-voting portal are displayed correctly.**


| [![google-chrome](../icons/google-chrome.png)](incognito_chrome.md) | [![Microsoft-Edge](../icons/microsoft-edge.png)](incognito_edge.md) | [![firefox](../icons/firefox.png)](incognito_firefox.md) | [![apple-safari](../icons/apple-safari.png)](incognito_safari.md) |
| ----------------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| [Google Chrome](incognito_chrome.md)           | [Edge](incognito_edge.md)                                         | [Firefox](incognito_firefox.md)                                   | [Safari](incognito_safari.md)                                    |

## Guide to Displaying and Synchronizing System Time

To enable voting, we recommend activating time synchronization on your device.

| [![Microsoft-Edge](../icons/time.png)](timecheck.md) |
| ------------------------------------------------------------ |
| [Guide to Displaying and Synchronizing System Time](timecheck.md)