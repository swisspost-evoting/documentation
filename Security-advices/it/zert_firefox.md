# Firefox

## Controllo del certificato


1. Fare un clic sul **simbolo del lucchetto** accanto alla barra degli indirizzi, contrassegnato da una freccia nell'immagine.
2. Fare un clic su **"Connessione sicura”**.
2. Fare un clic su **"Ulteriori informazioni”**.
3. Nel menu **Sicurezza**, fare clic su **"Mostra certificato”**.
4. Nella sezione **Impronte digitali**, confrontare il valore SHA-256 con quello della tessera elettorale. Deve **essere identico**.

**Screenshot Passo: 1**

![Screenshot Passo: 1](../img/rules/edge/de/edge_zert1.PNG)


**Nota:** L'indirizzo web si compone sempre come segue: ct.evoting.ch, dove "ct" sta per l'abbreviazione del rispettivo Cantone.
