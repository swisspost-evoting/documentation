# Accompanying documentation for the *Protocol of the Swiss Post Voting System - Computational Proof of Complete Verifiability and Privacy*

## What is the content of this document?

This document describes the cryptographic building blocks and shows how they work together to ensure verifiability and vote privacy. Moreover, it provides a mathematical proof that the cryptographic protocol achieves the desired security objectives under a minimal set of assumptions. The cryptographic proof is a central element of guaranteeing the security of a protocol in modern cryptography.

## What is a protocol?

A [security protocol](https://en.wikipedia.org/wiki/Cryptographic_protocol) (cryptographic protocol or encryption protocol) is an abstract or concrete protocol that performs a security-related function and applies cryptographic methods, often as sequences of cryptographic primitives.

## Why is a cryptographic proof important?

In line with the current best practice in cryptography, the Federal Chancellery requires a cryptographic proof of verifiability and privacy. To cite [Katz and Lindell](https://www.cs.umd.edu/~jkatz/imc.html): *Without proof that no adversary of the specified power can break the scheme, we are left only with our intuition that this is the case. Experience has shown that intuition in cryptography and computer security is disastrous. There are countless examples of unproven schemes that were broken, sometimes immediately and sometimes years after being presented or deployed.*


## Changelog 

An overview of all major changes within the document is available [here](CHANGELOG.md).

## Changes since publication in 2019

Since its initial publication in 2019, we have improved our cryptographic protocol to ensure that it is more:

- complete: we have significantly expanded the description of the underlying building blocks (mix net, zero-knowledge proofs, etc.).

- understandable: we have simplified the description of the protocol and aligned key, variable and algorithm names.

- rigorous: we have improved the coherence of the theorems and proofs; for instance, by taking into account multiple voting options.

Furthermore, we have merged in one single document the proofs of complete verifiability and vote secrecy.

## Limitations

Our cryptographic proof demonstrates that the Swiss Post Voting System guarantees complete verifiability and vote privacy.

However, no cryptographic protocol is unconditionally secure. The protocol of the Swiss Post Voting System bases its security objectives on a few assumptions. Even though these assumptions comply with the [Federal Chancellery's Ordinance on Electronic Voting](https://www.fedlex.admin.ch/eli/cc/2022/336/en), we will highlight some of them here for the sake of clarity and transparency:

- First, we are defending our security properties against an adversary that is polynomially bounded. An efficient, fault-tolerant quantum computer could break some of the mathematical assumptions underpinning the Swiss Post Voting System. Quantum-resistant e-voting protocols exist, but they are relatively recent, and their security properties are less understood. We consider quantum-resistance an area for future improvement of the protocol. [The Federal Chancellery's *Summary of the Expert Dialog 2020*, section 11.2.11 addresses this topic](https://www.newsd.admin.ch/newsd/message/attachments/63915.pdf).

- Second, for the purpose of providing vote secrecy, the protocol of the Swiss Post Voting System assumes that the attacker is not controlling the voting client. While we guarantee individual verifiability even if the voting client is malicious, we cannot use cryptography to prevent attackers from spying on the voter's choices on malicious clients. However, the Swiss Post Voting System prevents the server from breaking vote secrecy if at least one control component is honest.

- Third, the protocol of the Swiss Post Voting System assumes a trustworthy setup component. The setup component generates keys and codes in conjunction with the control components. By its nature, the setup component handles sensitive data, and the process for generating and sending voting cards must meet rigorous requirements. In general, e-voting systems aim to distribute trust and detect any attack or malfunction if at least one control component works correctly. However, we believe that there are certain limits for distributing the functionality of the setup and printing component: one cannot expect the voter to combine different code sheets by hand, and the costs of printing multiple code sheets in independent printing facilities would be currently prohibitive. For now, we propose to implement a return code scheme with a single setup and printing, which must put organizational and technical means into place to prevent leaking the secret codes. * The reduction of trust assumptions in the setup component is part of the [final report of the e-voting steering committee of cantons and confederation for the revision of the current conditions, Measure A.5](https://www.newsd.admin.ch/newsd/message/attachments/64680.pdf).

## Subgroup Generated by Small Primes (SGSP)

The Swiss Post Voting System's computational privacy proof relies on the Subgroup Generated by Small Primes (SGSP) problem. The SGSP problem is related to the Decisional Diffie-Hellmann (DDH) problem. However, the SGSP problem has been studied less in the academic literature than DDH.
Pierrick Gaudry from CNRS/LORIA, a renowned expert in discrete logarithm problems, wrote a [short article](./sgsp.pdf) discussing the SGSP problem.
