# Firefox

## Browserverlauf löschen in Firefox

1. Klicken Sie auf die **"drei horizontalen Linien"** oben rechts, im Screenshot mit einem Pfeil markiert.
2. Wählen Sie den Menüpunkt **Chronik**.
3. Klicken Sie auf den Menüpunkt **"Neuste Chronik löschen"**. (Tastenkombination: Strg + Umschalt + Entf)
4. Wählen Sie in der Dropdown Liste mindestens den Zeitraum, der den Vorgang Ihrer elektronischen Stimmabgabe umfasst. Wählen Sie zum Beispiel den Eintrag **"Letzte Stunde"**.
5. Klicken Sie auf die Menütaste **"Jetzt löschen"**.

**Screenshot Schritt: 1**

![Screenshot Schritt: 1](../img/rules/firefox/de/1.png)



