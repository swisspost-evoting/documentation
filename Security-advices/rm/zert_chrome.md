# Google Chrome

## Verifitgar il certificat

1. Cliccar sin il buttun **"Mussar infurmaziuns sin la pagina d'internet"** qua en il monitur cun la pitga.
2. Cliccai ussa sin il menu avert sin *** La colliaziun è segira"**.
3. Cliccai sin **"Il certificat è valaivel"**.
4. Uss vesais Vus en la nova fanestra sut il chavalier **"Allgemein"** las improntas da SHA-256. Impurtanta per Vus è l'impronta dal certificat. Cumparegliai per plaschair questa valur cun la valur inditgada sin Voss attest da votar. **El sto esser identic**.

**Screenshot Pass: 1**

![Screenshot Pass: 1](../img/rules/chrome/de/z1.png)


**Indicaziun:** L’adressa-web sa cumpona adina uschia: ct.evoting.ch, «ct» stat per l’abreviaziun dal chantun correspundent. 
